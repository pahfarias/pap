
package client;
import gui.WindowMain;

import java.io.*;
import java.net.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import requests.Request;



public class Client extends Thread{
	public static int port;
	public static String ip;
	public static Socket socket;
	public static WorkerThread workerThread;
	public static Thread gameThread;
	public static ReceiverThread receiver;
	public static BlockingQueue<Request> requestQueue = new ArrayBlockingQueue<>(100);
	public static ObjectOutputStream out;
	public static ObjectInputStream in;
	public static WindowMain window;
	public static String user;

	public Client() {
		connect();
		this.start();
	}

	public static void main(String args[]) {
		window = new WindowMain();
		new Client();
	}

	@Override
	public void run() {
		while(true){
			try {
				out.writeObject(requestQueue.take());
			} catch (Exception e) {
				connect();

			}
		}
	}

	public static void connect() {
		port = 6000;
		ip = "192.168.1.76";
		//ip = "127.0.0.1";
		try{
			socket = new Socket(ip, port);
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
			workerThread = new WorkerThread();
			receiver = new ReceiverThread();
		}catch(Exception e){
			WindowMain.connecting();
		}
	}
}
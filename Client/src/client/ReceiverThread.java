package client;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import responses.Response;

public class ReceiverThread extends Thread{
	
	public ReceiverThread() {
		this.start();
	}

	@Override
	public void run() {
		while(true){
			try {
				WorkerThread.responses.put((Response) Client.in.readObject());
			} catch (ClassNotFoundException | IOException e) {
				if( e.toString().trim().equals("java.net.SocketException: Connection reset"))
					Client.connect();
				else{
					System.out.println("Error: " + e);
					try {
						sleep(5000);
					} catch (InterruptedException e1) {}
					
				}	
			} catch (InterruptedException e) {System.out.println(e);
			}
		}
	}
	
}

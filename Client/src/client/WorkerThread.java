package client;
import game.Game;
import gui.Chat;
import gui.WindowMain;
import gui.WindowMenu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import javax.swing.JOptionPane;

import responses.Response;
import responses.ResponseBoolean;
import responses.ResponseChat;
import responses.ResponseChatGame;
import responses.ResponseColor;
import responses.ResponseEndGame;
import responses.ResponseFindFriend;
import responses.ResponseFriendRequest;
import responses.ResponseInvite;
import responses.ResponseMove;
import responses.ResponseRPS;
import responses.ResponseStartGame;

public class WorkerThread extends Thread{
	public static BlockingQueue<Response> responses = new ArrayBlockingQueue<>(100);

	public WorkerThread() {
		this.start();
	}

	@Override
	public void run() {
		while(true){
			try {
				processResponse(responses.take());
			} catch (InterruptedException e) {
			}
		}
	}

	private void processResponse(Response response) {
		
		if (response instanceof ResponseBoolean){
			responseBoolean((ResponseBoolean) response);
		}
		else if (response instanceof ResponseFindFriend){
			responseFindFriend((ResponseFindFriend) response);
		}
		else if (response instanceof ResponseFriendRequest){
			responseFriendRequest((ResponseFriendRequest) response);
		}
		else if (response instanceof ResponseChat) {
			responseChat((ResponseChat) response);
		}
		else if(response instanceof ResponseInvite){
			responseInvite((ResponseInvite) response);
		}
		else if(response instanceof ResponseStartGame){
			responseStartGame((ResponseStartGame) response);
		}
		else if(response instanceof ResponseRPS){
			responseRPS((ResponseRPS) response);
		}
		else if(response instanceof ResponseColor){
			responseColor((ResponseColor) response);
		}
		
		else if(response instanceof ResponseChatGame){
			responseChatGame((ResponseChatGame) response);
		}
		else if(response instanceof ResponseMove){
			responseMove((ResponseMove) response);
		}
		else if(response instanceof ResponseEndGame){
			responseEndGame((ResponseEndGame) response);
		}
	}

	private void responseEndGame(ResponseEndGame response) {
		WindowMenu.gameList.get(WindowMenu.gameList.size()-1).endGame(response.isWinner, response.reason);
	}

	private void responseMove(ResponseMove response) {
		WindowMenu.gameList.get(WindowMenu.gameList.size()-1).move(response.boccupied, response.woccupied);
	}

	private void responseChatGame(ResponseChatGame response) {
		WindowMenu.gameList.get(WindowMenu.gameList.size()-1).setChat(response.chat);
	}

	private void responseColor(ResponseColor response) {
		WindowMenu.gameList.get(WindowMenu.gameList.size()-1).setColor(response.color);
		
	}

	private void responseRPS(ResponseRPS response) {
		WindowMenu.gameList.get(WindowMenu.gameList.size()-1).checkRPS(response.rps);
	}

	private void responseStartGame(final ResponseStartGame response) {
		Client.gameThread = new Thread(new Runnable() {
			@Override
			public void run() {
				WindowMenu.startGame(response.id, response.opponent);
			}
		});
		Client.gameThread.start();
		
	}

	private void responseInvite(ResponseInvite response) {
		if(WindowMenu.notificationsList.size() != 0){
			boolean flag = true;
			for (int i = 0; i < WindowMenu.notificationsList.size(); i++) {
				if(WindowMenu.notificationsList.get(i) instanceof ResponseInvite){
					ResponseInvite notification = (ResponseInvite) WindowMenu.notificationsList.get(i);
					if (notification.user.equals(response.user)) {
						flag = false;
					}
				}
			}

			if(flag)
				WindowMenu.notificationsList.add(response);
		}
		else
			WindowMenu.notificationsList.add(response);
		WindowMenu.setNotifications();
	}

	private void responseChat(ResponseChat response) {
		Collections.sort((response.users));
		boolean isChat = false;
		for (Chat chat : WindowMenu.chats) {
			if( response.users.equals(chat.users) ){
				chat.setText(response.message);
				isChat = true;
			}
		}
		
		if(!isChat){
			WindowMenu.setChat(response.users, response.message);
		}
	}

	private void responseFriendRequest(ResponseFriendRequest response) {
		if(WindowMenu.notificationsList.size() != 0){
			boolean flag = true;
			for (int i = 0; i < WindowMenu.notificationsList.size(); i++) {
				if(WindowMenu.notificationsList.get(i) instanceof ResponseFriendRequest){
					ResponseFriendRequest notification = (ResponseFriendRequest) WindowMenu.notificationsList.get(i);
					if (notification.getIdRequest() == response.getIdRequest()) {
						flag = false;
					}
				}
			}

			if(flag)
				WindowMenu.notificationsList.add(response);
		}
		else
			WindowMenu.notificationsList.add(response);
		WindowMenu.setNotifications();
	}

	private void responseFindFriend(ResponseFindFriend response) {
		
		WindowMenu.setFriends(response);

	}

	private void responseBoolean(ResponseBoolean response) {
		if(response.isRegistered() && response.isSuccessful()){
			WindowMain.login();
		}
		else if(!response.isRegistered() && response.isSuccessful()){
			WindowMain.register();
		}
		else if(response.isRegistered()){
			WindowMain.noLogin();
		}
		else{
			WindowMain.noRegister();
		}
	}


}
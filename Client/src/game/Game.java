package game;
import gui.WindowMenu;

import java.awt.Font;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.spec.GCMParameterSpec;
import javax.swing.ImageIcon;

import org.lwjgl.Sys;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.gui.AbstractComponent;
import org.newdawn.slick.gui.ComponentListener;
import org.newdawn.slick.gui.MouseOverArea;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.opengl.SlickCallable;
import org.newdawn.slick.util.FontUtils;

import requests.RequestChatGame;
import requests.RequestCheckMate;
import requests.RequestColor;
import requests.RequestMove;
import requests.RequestRPS;
import requests.RequestSurrender;
import client.Client;

public class Game extends BasicGame{
	AppGameContainer appgc;
	int xpos, ypos, selXpos, selYpos;
	int rps = 0;
	int win = -1;
	int rpsOpp = 0;
	int id;
	int color = -1;
	int[][] boccupied = new int[16][2], woccupied = new int[16][2];
	boolean clicked, isRPS, isTurn, selected = false;
	Font font;
	TrueTypeFont fontGisha, fontStencilStd, fontCracked, fontGishaRed;
	TextField textf;
	ArrayList<String> chat = new ArrayList<>();
	int resX, resY;
	Image rock, paper, scissors, loserRps, winnerRps;
	boolean surrPressed = false;
	int time;
	int runningtime = 0;
	String opponent = "";
	boolean check = false, check2 = false;
	boolean surrender = false, noPressed = false, yesPressed = false;
	boolean isWinner;
	int reason = -1;
	boolean end = false;
	public boolean continuePressed = false;
	public boolean disable = false;
	public int[] checkPiece = new int[2];
	public int checkPieceIndex;
	public boolean checkMate = false;
	public boolean staleMate = false;
	public boolean writeCheck;
	public Game(String gamename)
	{
		super(gamename);
	}
	public void setColor(int c){
		color = c;
		isRPS = false;

	}
	public void checkRPS(int rpsOpp){
		this.rpsOpp = rpsOpp;
		switch (rps) {
		case 1:
			if(rpsOpp == 1)
				win=2;
			else if(rpsOpp == 2)
				win = 0;
			else
				win = 1;
			break;
		case 2:
			if(rpsOpp == 1)
				win = 1;
			else if(rpsOpp == 2)
				win=2;
			else
				win = 0;
			break;
		case 3:
			if(rpsOpp == 1)
				win = 0;
			else if(rpsOpp == 2)
				win = 1;
			else
				win=2;
			break;
		}

		if(win == 2){
			rps = 0;
			this.rpsOpp = 0;
		}

	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		appgc.setUpdateOnlyWhenVisible(false);
		gc.setAlwaysRender(true);
		isRPS = true;
		font = new Font("Gisha", Font.PLAIN, sclY(20));
		fontGisha = new TrueTypeFont(font, true);
		font = new Font("Gisha", Font.BOLD, sclY(20));
		fontGishaRed = new TrueTypeFont(font, true);
		font = new Font("Stencil Std", Font.PLAIN, sclY(36));
		fontStencilStd = new TrueTypeFont(font, true);
		font = new Font("Cracked Johnnie", Font.PLAIN, sclY(28));
		fontCracked = new TrueTypeFont(font, true);
		textf = new TextField(gc, fontGisha, sclX(850), sclY(785),sclX(215), sclY(35));
		textf.addListener(new ComponentListener() {
			public void componentActivated(AbstractComponent arg0) {
				if(!textf.getText().isEmpty())
					Client.requestQueue.add(new RequestChatGame(id, Client.user, textf.getText()));
				textf.setText("");
			}
		});

		textf.setBorderColor(null);
		textf.setBackgroundColor(null);
		boccupied[0][0] = -2;
		rock = new Image("game/rock.png");
		paper = new Image("game/paper.png");
		scissors = new Image("game/scissors.png");
		loserRps = new Image("game/loserRPS.png");
		winnerRps = new Image("game/winRPS.png");
		time = 90;
	}

	@Override
	public void mousePressed(int button, int x, int y) {
		//super.mousePressed(button, x, y);
		if(end && reason == 1){
			if(button == 0 && x <= sclX(770) && x >= sclX(340) && y <= sclY(775) && y >= sclY(660))
				continuePressed = true;
		}
		
		else if(end){
			if(button == 0 && x <= sclX(1093) && x >= sclX(840) && y <= sclY(893) && y >= sclY(835))
				continuePressed = true;
		}

		else if(surrender){
			if(button == 0 && x <= sclX(520) && x >= sclX(310) && y <= sclY(539) && y >= sclY(469))
				yesPressed = true;
			else if(button == 0 && x <= sclX(790) && x >= sclX(580) && y <= sclY(540) && y >= sclY(470))
				noPressed = true;
		}

		else if(button == 0 && x <= sclX(1060) && x >= sclX(810) && y <= sclY(892) && y >= sclY(835))
			surrPressed = true;
	}

	@Override
	public void mouseReleased(int button, int x, int y) {
		//super.mouseReleased(button, x, y);
		if(end && reason == 1){
			if(button == 0 && x <= sclX(770) && x >= sclX(340) && y <= sclY(775) && y >= sclY(660) && continuePressed)
				WindowMenu.endGame();
			continuePressed = false;
		}

		else if(end){
			if(button == 0 && x <= sclX(1093) && x >= sclX(840) && y <= sclY(893) && y >= sclY(835) && continuePressed)
				WindowMenu.endGame();
			continuePressed = false;
		}

		if(surrender){
			if(button == 0 && x <= sclX(520) && x >= sclX(310) && y <= sclY(539) && y >= sclY(469) && yesPressed)
				Client.requestQueue.add(new RequestSurrender(id, Client.user));
			surrender = false;
			noPressed = false;
			yesPressed = false;
		}

		else if(surrPressed){
			if(x <= sclX(1060) && x >= sclX(810) && y <= sclY(892) && y >= sclY(835)){
				surrender = true;
			}
			surrPressed = false;
		}

	}

	public int sclX(int x){
		x = (int) Math.floor(x*resX/1100);
		return x;
	}

	public int sclY(int y){
		y = (int) Math.floor(y*resY/900);
		return y;
	}

	@Override
	public void update(GameContainer gc, int i) throws SlickException {
		if (disable){
			//appgc.destroy();
			//gc.setForceExit(false);
			appgc.setForceExit(false);
			appgc.exit();
			return;
			//gc.exit();	
		}

		Input input = gc.getInput();

		if (input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {

			xpos = input.getMouseX();
			ypos = input.getMouseY();
			clicked = true;
		}

		if(end){

		}

		else if(isRPS){
			if(clicked && rps == 0){
				if(ypos >= sclY(15) && ypos <= sclY(375)){
					if(xpos >= sclX(15) && xpos <= sclX(360)){ // Rock
						rps = 1;
						Client.requestQueue.add(new RequestRPS(rps, id, Client.user));
					}
					else if(xpos >= sclX(380) && xpos <= sclX(725)){ // Paper
						rps = 2;
						Client.requestQueue.add(new RequestRPS(rps, id, Client.user));
					}
					else if(xpos >= sclX(745) && xpos <= sclX(1090)){ //Scissors
						rps = 3;
						Client.requestQueue.add(new RequestRPS(rps, id, Client.user));
					}
				}
				clicked = false;
			}

			else if(clicked && color == -1 && win == 1){
				if(ypos >= sclY(275) && ypos <= sclY(470)){
					if(xpos >= sclX(265) && xpos <= sclX(455)){ // BLACK
						color = 0;
						Client.requestQueue.add(new RequestColor(color, id,Client.user));
					}
					else if(xpos >= sclX(645) && xpos <= sclX(835)){ // WHITE
						color = 1;
						Client.requestQueue.add(new RequestColor(color, id,Client.user));
					}
				}
			}
		}

		else{

			if(boccupied[0][0] == -2)
				initPieces();

			runningtime+=i;
			if(runningtime/1000 >= 1f){
				time--;
				runningtime = 0;
			}

			textf.setAcceptingInput(!surrender);

			if(!surrender){
				if(!check && isTurn){
					isCheck();
					if(check){

						if(isCheckMate()){
							checkMate = true;
							Client.requestQueue.add(new RequestCheckMate(id,Client.user));
						}
						else{
							isCheck();
							writeCheck = true;
						}
					}
				}
				/*else if(isCheckMate()){
					staleMate = true;
					System.out.println("STALEMATE");
				}*/


				if(!checkMate){

					if(check && writeCheck && isTurn){
						setChat("YOU ARE IN CHECK");
						writeCheck = false;
					}

					if(clicked && isTurn){
						for (int x = sclX(115), y = sclY(115), z = 0; z < 8; z++, x+=sclX(100), y+=sclY(100)) {
							if(xpos < x){
								xpos = z;
								x = -2000;
							}

							if(ypos < y){
								ypos = z;
								y = -2000;
							} 
						}

						if(ypos <= 7 && xpos <= 7){

							boolean flag = true;

							for (int j = 0; j < boccupied.length; j++) {
								if(boccupied[j][0] == xpos && boccupied[j][1] == ypos && color == 0|| woccupied[j][0] == xpos && woccupied[j][1] == ypos && color == 1){
									flag = false;
								}
							}

							if(selected == true && selXpos == xpos && selYpos == ypos){
								selected = false;

							}

							else if(selected && flag && color == 1){  /* PE�AS BRANCAS */
								int b = -1;

								for (int l = 0; l < boccupied.length; l++) 
									if(boccupied[l][0] == xpos && boccupied[l][1] == ypos)
										b = l;

								for (int j = 0; j < 2; j++) {

									if(selXpos == woccupied[j][0] && selYpos == woccupied[j][1]){ 
										if(ypos == woccupied[j][1] && xpos<=7){				// TORRES BRANCAS - LINHAS
											for (int l = 0; l < woccupied.length; l++) {
												if(xpos < woccupied[j][0])
													for (int x = xpos+1; x < woccupied[j][0]; x++) {
														if(woccupied[l][0] == x && woccupied[l][1] == ypos || boccupied[l][0] == x && boccupied[l][1] == ypos)
															flag=false;
													}
												else
													for (int x = xpos-1; x > woccupied[j][0]; x--) {
														if(woccupied[l][0] == x && woccupied[l][1] == ypos || boccupied[l][0] == x && boccupied[l][1] == ypos)
															flag=false;
													}
											}

											if(flag){
												woccupied[j][0] = xpos;
												check = false;
												isCheck();
												if(check){
													woccupied[j][0] = selXpos;
													check = false;
													flag = false;
													isCheck();
												}
												else if(b != -1)
													boccupied[b][0] = -1;
											}

										}

										else if(xpos == woccupied[j][0] && ypos<=7){   // TORRES BRANCAS - COLUNAS
											for (int l = 0; l < woccupied.length; l++) {
												if(ypos < woccupied[j][1])
													for (int y = ypos+1; y < woccupied[j][1]; y++) {
														if(woccupied[l][0] == xpos && woccupied[l][1] == y || boccupied[l][0] == xpos && boccupied[l][1] == y)
															flag=false;
													}
												else
													for (int y = ypos-1; y > woccupied[j][1]; y--) {
														if(woccupied[l][0] == xpos && woccupied[l][1] == y || boccupied[l][0] == xpos && boccupied[l][1] == y)
															flag=false;
													}
											}

											if(flag){
												woccupied[j][1] = ypos;
												check = false;
												isCheck();
												if(check){
													woccupied[j][1] = selYpos;
													check = false;
													flag = false;
													isCheck();
												}

												else if(b != -1)
													boccupied[b][0] = -1;
											}

										}

										else
											flag = false;
									}

									else if(selXpos == woccupied[j+2][0] && selYpos == woccupied[j+2][1]){   // CAVALOS BRANCOS
										if(ypos == woccupied[j+2][1]-2 && xpos == woccupied[j+2][0]+1){
											woccupied[j+2][0] = xpos;
											woccupied[j+2][1] = ypos;
											check = false;
											isCheck();
											if(check){
												woccupied[j+2][0] = selXpos;
												woccupied[j+2][1] = selYpos;
												check = false;
												flag = false;
												isCheck();
											}

											else if(b != -1)
												boccupied[b][0] = -1;
										}

										else if(ypos == woccupied[j+2][1]-2 && xpos == woccupied[j+2][0]-1){
											woccupied[j+2][0] = xpos;
											woccupied[j+2][1] = ypos;
											check = false;
											isCheck();
											if(check){
												woccupied[j+2][0] = selXpos;
												woccupied[j+2][1] = selYpos;
												check = false;
												flag = false;
												isCheck();
											}

											else if(b != -1)
												boccupied[b][0] = -1;
										}	
										else if(ypos == woccupied[j+2][1]+2 && xpos == woccupied[j+2][0]-1){
											woccupied[j+2][0] = xpos;
											woccupied[j+2][1] = ypos;
											check = false;
											isCheck();
											if(check){
												woccupied[j+2][0] = selXpos;
												woccupied[j+2][1] = selYpos;
												check = false;
												flag = false;
												isCheck();
											}

											else if(b != -1)
												boccupied[b][0] = -1;
										}

										else if(ypos == woccupied[j+2][1]+2 && xpos == woccupied[j+2][0]+1){
											woccupied[j+2][0] = xpos;
											woccupied[j+2][1] = ypos;
											check = false;
											isCheck();
											if(check){
												woccupied[j+2][0] = selXpos;
												woccupied[j+2][1] = selYpos;
												check = false;
												flag = false;
												isCheck();
											}

											else if(b != -1)
												boccupied[b][0] = -1;
										}

										else if(ypos == woccupied[j+2][1]-1 && xpos == woccupied[j+2][0]+2){
											woccupied[j+2][0] = xpos;
											woccupied[j+2][1] = ypos;
											check = false;
											isCheck();
											if(check){
												woccupied[j+2][0] = selXpos;
												woccupied[j+2][1] = selYpos;
												check = false;
												flag = false;
												isCheck();
											}

											else if(b != -1)
												boccupied[b][0] = -1;
										}

										else if(ypos == woccupied[j+2][1]-1 && xpos == woccupied[j+2][0]-2){
											woccupied[j+2][0] = xpos;
											woccupied[j+2][1] = ypos;
											check = false;
											isCheck();
											if(check){
												woccupied[j+2][0] = selXpos;
												woccupied[j+2][1] = selYpos;
												check = false;
												flag = false;
												isCheck();
											}

											else if(b != -1)
												boccupied[b][0] = -1;
										}

										else if(ypos == woccupied[j+2][1]+1 && xpos == woccupied[j+2][0]-2){
											woccupied[j+2][0] = xpos;
											woccupied[j+2][1] = ypos;
											check = false;
											isCheck();
											if(check){
												woccupied[j+2][0] = selXpos;
												woccupied[j+2][1] = selYpos;
												check = false;
												flag = false;
												isCheck();
											}

											else if(b != -1)
												boccupied[b][0] = -1;
										}

										else if(ypos == woccupied[j+2][1]+1 && xpos == woccupied[j+2][0]+2){
											woccupied[j+2][0] = xpos;
											woccupied[j+2][1] = ypos;
											check = false;
											isCheck();
											if(check){
												woccupied[j+2][0] = selXpos;
												woccupied[j+2][1] = selYpos;
												check = false;
												flag = false;
												isCheck();
											}

											else if(b != -1)
												boccupied[b][0] = -1;
										}

										else
											flag = false;



									}

									else if(selXpos == woccupied[j+4][0] && selYpos == woccupied[j+4][1]){    // BISPOS BRANCOS
										boolean flag2 = true;
										for (int g = 0; g <= 7; g ++) {
											if(
													(xpos == woccupied[j+4][0] + g && ypos== woccupied[j+4][1] + g)||
													(xpos == woccupied[j+4][0] - g && ypos== woccupied[j+4][1] - g)||
													(xpos == woccupied[j+4][0] + g && ypos== woccupied[j+4][1] - g)||
													(xpos == woccupied[j+4][0] - g && ypos== woccupied[j+4][1] + g)){

												flag2 = false;

												for (int l = 0; l < woccupied.length; l++) {
													if(xpos < woccupied[j+4][0] && ypos < woccupied[j+4][1])
														for (int x = xpos+1, y = ypos+1; x < woccupied[j+4][0]; x++, y++) {
															if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
																flag=false;
														}
													else if(xpos < woccupied[j+4][0] && ypos > woccupied[j+4][1])
														for (int x = xpos+1, y = ypos-1; x < woccupied[j+4][0]; x++, y--) {
															if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
																flag=false;
														}
													else if(xpos > woccupied[j+4][0] && ypos < woccupied[j+4][1])
														for (int x = xpos-1, y = ypos+1; x > woccupied[j+4][0]; x--, y++) {
															if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
																flag=false;
														}
													else if(xpos > woccupied[j+4][0] && ypos > woccupied[j+4][1])
														for (int x = xpos-1, y = ypos-1; x > woccupied[j+4][0]; x--, y--) {
															if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
																flag=false;

														}
													if(woccupied[l][0] == xpos && woccupied[l][1] == ypos)
														flag=false;
												}

												if(flag){
													woccupied[j+4][0] = xpos;
													woccupied[j+4][1] = ypos;
													check = false;
													isCheck();
													if(check){
														woccupied[j+4][0] = selXpos;
														woccupied[j+4][1] = selYpos;
														check = false;
														flag = false;
														isCheck();
													}

													else if(b != -1)
														boccupied[b][0] = -1;

												}
											}
										}

										if(flag2)
											flag = false;

									}

								}

								/* RAINHA BRANCA */

								if(selXpos == woccupied[7][0] && selYpos == woccupied[7][1]){
									if(xpos != woccupied[7][0] && ypos != woccupied[7][1]){
										boolean flag2 = true;

										for (int g = 0; g <= 7; g++) {
											if(
													(xpos == woccupied[7][0] + g && ypos== woccupied[7][1] + g)||
													(xpos == woccupied[7][0] - g && ypos== woccupied[7][1] - g)||
													(xpos == woccupied[7][0] + g && ypos== woccupied[7][1] - g)||
													(xpos == woccupied[7][0] - g && ypos== woccupied[7][1] + g)){

												flag2 = false;

												for (int l = 0; l < woccupied.length; l++) {
													if(xpos < woccupied[7][0] && ypos < woccupied[7][1])
														for (int x = xpos+1, y = ypos+1; x < woccupied[7][0]; x++, y++) {
															if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
																flag=false;
														}
													else if(xpos < woccupied[7][0] && ypos > woccupied[7][1])
														for (int x = xpos+1, y = ypos-1; x < woccupied[7][0]; x++, y--) {
															if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
																flag=false;
														}
													else if(xpos > woccupied[7][0] && ypos < woccupied[7][1])
														for (int x = xpos-1, y = ypos+1; x > woccupied[7][0]; x--,y++) {
															if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
																flag=false;
														}
													else if(xpos > woccupied[7][0] && ypos > woccupied[7][1])
														for (int x = xpos-1, y = ypos-1; x > woccupied[7][0]; x--, y--) {
															if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
																flag=false;

														}
												}

												if(flag){
													woccupied[7][0] = xpos;
													woccupied[7][1] = ypos;
													check = false;
													isCheck();
													if(check){
														woccupied[7][0] = selXpos;
														woccupied[7][1] = selYpos;
														check = false;
														flag = false;
														isCheck();
													}

													else if(b != -1)
														boccupied[b][0] = -1;
												}
											}	
										}

										if(flag2)
											flag = false;
									}

									else{
										if(ypos == woccupied[7][1] && xpos<=7){
											for (int l = 0; l < woccupied.length; l++) {
												if(xpos < woccupied[7][0])
													for (int x = xpos + 1; x < woccupied[7][0]; x++) {
														if(woccupied[l][0] == x && woccupied[l][1] == ypos || boccupied[l][0] == x && boccupied[l][1] == ypos)
															flag=false;
													}
												else
													for (int x = xpos - 1 ; x > woccupied[7][0]; x--) {
														if(woccupied[l][0] == x && woccupied[l][1] == ypos || boccupied[l][0] == x && boccupied[l][1] == ypos)
															flag=false;
													}
											}

											if(flag){
												woccupied[7][0] = xpos;
												check = false;
												isCheck();
												if(check){
													woccupied[7][0] = selXpos;
													check = false;
													flag = false;
													isCheck();
												}

												else if(b != -1)
													boccupied[b][0] = -1;
											}
										}
										else if(xpos == woccupied[7][0] && ypos<=7){

											for (int l = 0; l < woccupied.length; l++) {
												if(ypos < woccupied[7][1])
													for (int y = ypos +1 ; y < woccupied[7][1]; y++) {
														if(woccupied[l][0] == xpos && woccupied[l][1] == y || boccupied[l][0] == xpos && boccupied[l][1] == y)
															flag=false;
													}
												else
													for (int y = ypos - 1; y > woccupied[7][1]; y--) {
														if(woccupied[l][0] == xpos && woccupied[l][1] == y || boccupied[l][0] == xpos && boccupied[l][1] == y)
															flag=false;
													}
											}
											if(flag){
												woccupied[7][1] = ypos;
												check = false;
												isCheck();
												if(check){
													woccupied[7][1] = selYpos;
													check = false;
													flag = false;
													isCheck();
												}

												else if(b != -1)
													boccupied[b][0] = -1;
											}
										}

										else
											flag = false;
									}
								}

								/* REI BRANCO */

								else if(selXpos == woccupied[6][0] && selYpos == woccupied[6][1]){
									if(
											(xpos == woccupied[6][0] + 1 || xpos == woccupied[6][0] - 1 || xpos == woccupied[6][0]) && 
											(ypos == woccupied[6][1] + 1 || ypos == woccupied[6][1] - 1 || ypos == woccupied[6][1])){

										woccupied[6][0] = xpos;
										woccupied[6][1] = ypos;
										check = false;
										isCheck();

										if(check){
											woccupied[6][0] = selXpos;
											woccupied[6][1] = selYpos;
											check = false;
											flag = false;
											isCheck();
										}

										else if(b != -1)
											boccupied[b][0] = -1;
									}

									else
										flag = false;
								}

								/* PE�ES BRANCOS */

								else{
									for (int g = 0; g <= 7; g++) {
										if(selXpos == woccupied[g+8][0] && selYpos == woccupied[g+8][1]){
											if((woccupied[g+8][1] == 6 && xpos == woccupied[g+8][0] && ypos == woccupied[g+8][1] - 2) || (xpos == woccupied[g+8][0] && ypos == woccupied[g+8][1] - 1 && ypos >= 0)){
												boolean flag2 = true;
												for (int l = 0; l < woccupied.length; l++) {
													if(woccupied[l][0] == xpos && woccupied[l][1] == ypos || boccupied[l][0] == xpos && boccupied[l][1] == ypos ||
															woccupied[g+8][1] - ypos == 2 && woccupied[l][0] == xpos && woccupied[l][1] == woccupied[g+8][1]- 1 || woccupied[g+8][1] - ypos == 2 && boccupied[l][0] == xpos && boccupied[l][1] == woccupied[g+8][1]-100)
														flag2=false;
												}

												if(flag2){
													woccupied[g+8][1] = ypos;
													check = false;
													isCheck();
													if(check){
														woccupied[g+8][1] = selYpos;
														check = false;
														flag = false;
														isCheck();
													}
												}
												else
													flag = false;
											}

											else if((xpos == woccupied[g+8][0] + 1 || xpos == woccupied[g+8][0] - 1) && ypos == woccupied[g+8][1] - 1 && ypos >= 0 && b != -1){
												woccupied[g+8][1] = ypos;
												woccupied[g+8][0] = xpos;
												check = false;
												isCheck();
												if(check){
													woccupied[g+8][0] = selXpos;
													woccupied[g+8][1] = selYpos;
													check = false;
													flag = false;
													isCheck();
												}

												else
													boccupied[b][0] = -1;

											}
											else	
												flag = false;
										}
									}
								}

								xpos = 0;
								ypos = 0;

								if(flag){
									check = false;
									check2 = false;
									System.out.println("sent");
									Client.requestQueue.add(new RequestMove(id,Client.user, boccupied, woccupied));
									selected = false;
									isTurn = false;
								}
							}

							else if(selected && flag && color == 0){  /* PE�AS PRETAS */
								int b = -1;

								for (int l = 0; l < woccupied.length; l++) 
									if(woccupied[l][0] == xpos && woccupied[l][1] == ypos)
										b = l;

								for (int j = 0; j < 2; j++) {

									if(selXpos == boccupied[j][0] && selYpos == boccupied[j][1]){ 
										if(ypos == boccupied[j][1] && xpos<=7){				// TORRES PRETAS - LINHAS
											for (int l = 0; l < boccupied.length; l++) {
												if(xpos < boccupied[j][0])
													for (int x = xpos+1; x < boccupied[j][0]; x++) {
														if(boccupied[l][0] == x && boccupied[l][1] == ypos || woccupied[l][0] == x && woccupied[l][1] == ypos)
															flag=false;
													}
												else
													for (int x = xpos-1; x > boccupied[j][0]; x--) {
														if(boccupied[l][0] == x && boccupied[l][1] == ypos || woccupied[l][0] == x && woccupied[l][1] == ypos)
															flag=false;
													}
											}

											if(flag){
												boccupied[j][0] = xpos;
												check = false;
												isCheck();
												if(check){
													boccupied[j][0] = selXpos;
													check = false;
													flag = false;
													isCheck();
												}

												else if(b != -1)
													woccupied[b][0] = -1;
											}

										}

										else if(xpos == boccupied[j][0] && ypos<=7){   // TORRES PRETAS - COLUNAS
											for (int l = 0; l < boccupied.length; l++) {
												if(ypos < boccupied[j][1])
													for (int y = ypos+1; y < boccupied[j][1]; y++) {
														if(boccupied[l][0] == xpos && boccupied[l][1] == y || woccupied[l][0] == xpos && woccupied[l][1] == y)
															flag=false;
													}
												else
													for (int y = ypos-1; y > boccupied[j][1]; y--) {
														if(boccupied[l][0] == xpos && boccupied[l][1] == y || woccupied[l][0] == xpos && woccupied[l][1] == y)
															flag=false;
													}
											}

											if(flag){
												boccupied[j][1] = ypos;
												check = false;
												isCheck();
												if(check){
													boccupied[j][1] = selYpos;
													check = false;
													flag = false;
													isCheck();
												}

												else if(b != -1)
													woccupied[b][0] = -1;
											}

										}

										else
											flag = false;
									}

									else if(selXpos == boccupied[j+2][0] && selYpos == boccupied[j+2][1]){   // CAVALOS PRETOS
										if(ypos == boccupied[j+2][1]-2 && xpos == boccupied[j+2][0]+1){
											boccupied[j+2][0] = xpos;
											boccupied[j+2][1] = ypos;
											check = false;
											isCheck();
											if(check){
												boccupied[j+2][0] = selXpos;
												boccupied[j+2][1] = selYpos;
												check = false;
												flag = false;
												isCheck();
											}
											else if(b != -1)
												woccupied[b][0] = -1;
										}

										else if(ypos == boccupied[j+2][1]-2 && xpos == boccupied[j+2][0]-1){
											boccupied[j+2][0] = xpos;
											boccupied[j+2][1] = ypos;
											check = false;
											isCheck();
											if(check){
												boccupied[j+2][0] = selXpos;
												boccupied[j+2][1] = selYpos;
												check = false;
												flag = false;
												isCheck();
											}
											else if(b != -1)
												woccupied[b][0] = -1;
										}	
										else if(ypos == boccupied[j+2][1]+2 && xpos == boccupied[j+2][0]-1){
											boccupied[j+2][0] = xpos;
											boccupied[j+2][1] = ypos;
											check = false;
											isCheck();
											if(check){
												boccupied[j+2][0] = selXpos;
												boccupied[j+2][1] = selYpos;
												check = false;
												flag = false;
												isCheck();
											}
											else if(b != -1)
												woccupied[b][0] = -1;
										}

										else if(ypos == boccupied[j+2][1]+2 && xpos == boccupied[j+2][0]+1){
											boccupied[j+2][0] = xpos;
											boccupied[j+2][1] = ypos;
											check = false;
											isCheck();
											if(check){
												boccupied[j+2][0] = selXpos;
												boccupied[j+2][1] = selYpos;
												check = false;
												flag = false;
												isCheck();
											}
											else if(b != -1)
												woccupied[b][0] = -1;
										}

										else if(ypos == boccupied[j+2][1]-1 && xpos == boccupied[j+2][0]+2){
											boccupied[j+2][0] = xpos;
											boccupied[j+2][1] = ypos;
											check = false;
											isCheck();
											if(check){
												boccupied[j+2][0] = selXpos;
												boccupied[j+2][1] = selYpos;
												check = false;
												flag = false;
												isCheck();
											}
											else if(b != -1)
												woccupied[b][0] = -1;
										}

										else if(ypos == boccupied[j+2][1]-1 && xpos == boccupied[j+2][0]-2){
											boccupied[j+2][0] = xpos;
											boccupied[j+2][1] = ypos;
											check = false;
											isCheck();
											if(check){
												boccupied[j+2][0] = selXpos;
												boccupied[j+2][1] = selYpos;
												check = false;
												flag = false;
												isCheck();
											}
											else if(b != -1)
												woccupied[b][0] = -1;
										}

										else if(ypos == boccupied[j+2][1]+1 && xpos == boccupied[j+2][0]-2){
											boccupied[j+2][0] = xpos;
											boccupied[j+2][1] = ypos;
											check = false;
											isCheck();
											if(check){
												boccupied[j+2][0] = selXpos;
												boccupied[j+2][1] = selYpos;
												check = false;
												flag = false;
												isCheck();
											}
											else if(b != -1)
												woccupied[b][0] = -1;
										}

										else if(ypos == boccupied[j+2][1]+1 && xpos == boccupied[j+2][0]+2){
											boccupied[j+2][0] = xpos;
											boccupied[j+2][1] = ypos;
											check = false;
											isCheck();
											if(check){
												boccupied[j+2][0] = selXpos;
												boccupied[j+2][1] = selYpos;
												check = false;
												flag = false;
												isCheck();
											}
											else if(b != -1)
												woccupied[b][0] = -1;
										}

										else
											flag = false;



									}

									else if(selXpos == boccupied[j+4][0] && selYpos == boccupied[j+4][1]){    // BISPOS PRETOS
										boolean flag2 = true;
										for (int g = 0; g <= 7; g ++) {
											if(
													(xpos == boccupied[j+4][0] + g && ypos== boccupied[j+4][1] + g)||
													(xpos == boccupied[j+4][0] - g && ypos== boccupied[j+4][1] - g)||
													(xpos == boccupied[j+4][0] + g && ypos== boccupied[j+4][1] - g)||
													(xpos == boccupied[j+4][0] - g && ypos== boccupied[j+4][1] + g)){

												flag2 = false;

												for (int l = 0; l < boccupied.length; l++) {
													if(xpos < boccupied[j+4][0] && ypos < boccupied[j+4][1])
														for (int x = xpos+1, y = ypos+1; x < boccupied[j+4][0]; x++, y++) {
															if(boccupied[l][0] == x && boccupied[l][1] == y || woccupied[l][0] == x && woccupied[l][1] == y)
																flag=false;
														}
													else if(xpos < boccupied[j+4][0] && ypos > boccupied[j+4][1])
														for (int x = xpos+1, y = ypos-1; x < boccupied[j+4][0]; x++, y--) {
															if(boccupied[l][0] == x && boccupied[l][1] == y || woccupied[l][0] == x && woccupied[l][1] == y)
																flag=false;
														}
													else if(xpos > boccupied[j+4][0] && ypos < boccupied[j+4][1])
														for (int x = xpos-1, y = ypos+1; x > boccupied[j+4][0]; x--, y++) {
															if(boccupied[l][0] == x && boccupied[l][1] == y || woccupied[l][0] == x && woccupied[l][1] == y)
																flag=false;
														}
													else if(xpos > boccupied[j+4][0] && ypos > boccupied[j+4][1])
														for (int x = xpos-1, y = ypos-1; x > boccupied[j+4][0]; x--, y--) {
															if(boccupied[l][0] == x && boccupied[l][1] == y || woccupied[l][0] == x && woccupied[l][1] == y)
																flag=false;

														}
													if(boccupied[l][0] == xpos && boccupied[l][1] == ypos)
														flag=false;
												}

												if(flag){
													boccupied[j+4][0] = xpos;
													boccupied[j+4][1] = ypos;
													check = false;
													isCheck();
													if(check){
														boccupied[j+4][0] = selXpos;
														boccupied[j+4][1] = selYpos;
														check = false;
														flag = false;
														isCheck();
													}

													else if(b != -1)
														woccupied[b][0] = -1;

												}
											}
										}

										if(flag2)
											flag = false;

									}

								}

								/* RAINHA PRETA */

								if(selXpos == boccupied[7][0] && selYpos == boccupied[7][1]){
									if(xpos != boccupied[7][0] && ypos != boccupied[7][1]){
										boolean flag2 = true;

										for (int g = 0; g <= 7; g++) {
											if(
													(xpos == boccupied[7][0] + g && ypos== boccupied[7][1] + g)||
													(xpos == boccupied[7][0] - g && ypos== boccupied[7][1] - g)||
													(xpos == boccupied[7][0] + g && ypos== boccupied[7][1] - g)||
													(xpos == boccupied[7][0] - g && ypos== boccupied[7][1] + g)){

												flag2 = false;

												for (int l = 0; l < boccupied.length; l++) {
													if(xpos < boccupied[7][0] && ypos < boccupied[7][1])
														for (int x = xpos+1, y = ypos+1; x < boccupied[7][0]; x++, y++) {
															if(boccupied[l][0] == x && boccupied[l][1] == y || woccupied[l][0] == x && woccupied[l][1] == y)
																flag=false;
														}
													else if(xpos < boccupied[7][0] && ypos > boccupied[7][1])
														for (int x = xpos+1, y = ypos-1; x < boccupied[7][0]; x++, y--) {
															if(boccupied[l][0] == x && boccupied[l][1] == y || woccupied[l][0] == x && woccupied[l][1] == y)
																flag=false;
														}
													else if(xpos > boccupied[7][0] && ypos < boccupied[7][1])
														for (int x = xpos-1, y = ypos+1; x > boccupied[7][0]; x--,y++) {
															if(boccupied[l][0] == x && boccupied[l][1] == y || woccupied[l][0] == x && woccupied[l][1] == y)
																flag=false;
														}
													else if(xpos > boccupied[7][0] && ypos > boccupied[7][1])
														for (int x = xpos-1, y = ypos-1; x > boccupied[7][0]; x--, y--) {
															if(boccupied[l][0] == x && boccupied[l][1] == y || woccupied[l][0] == x && woccupied[l][1] == y)
																flag=false;

														}
												}

												if(flag){
													boccupied[7][0] = xpos;
													boccupied[7][1] = ypos;
													check = false;
													isCheck();
													if(check){
														boccupied[7][0] = selXpos;
														boccupied[7][1] = selYpos;
														check = false;
														flag = false;
														isCheck();
													}

													else if(b != -1)
														woccupied[b][0] = -1;
												}
											}	
										}

										if(flag2)
											flag = false;
									}

									else{
										if(ypos == boccupied[7][1] && xpos<=7){
											for (int l = 0; l < boccupied.length; l++) {
												if(xpos < boccupied[7][0])
													for (int x = xpos + 1; x < boccupied[7][0]; x++) {
														if(boccupied[l][0] == x && boccupied[l][1] == ypos || woccupied[l][0] == x && woccupied[l][1] == ypos)
															flag=false;
													}
												else
													for (int x = xpos - 1 ; x > boccupied[7][0]; x--) {
														if(boccupied[l][0] == x && boccupied[l][1] == ypos || woccupied[l][0] == x && woccupied[l][1] == ypos)
															flag=false;
													}
											}

											if(flag){
												boccupied[7][0] = xpos;
												check = false;
												isCheck();
												if(check){
													boccupied[7][0] = selXpos;
													check = false;
													flag = false;
													isCheck();
												}

												else if(b != -1)
													woccupied[b][0] = -1;
											}
										}
										else if(xpos == boccupied[7][0] && ypos<=7){

											for (int l = 0; l < boccupied.length; l++) {
												if(ypos < boccupied[7][1])
													for (int y = ypos +1 ; y < boccupied[7][1]; y++) {
														if(boccupied[l][0] == xpos && boccupied[l][1] == y || woccupied[l][0] == xpos && woccupied[l][1] == y)
															flag=false;
													}
												else
													for (int y = ypos - 1; y > boccupied[7][1]; y--) {
														if(boccupied[l][0] == xpos && boccupied[l][1] == y || woccupied[l][0] == xpos && woccupied[l][1] == y)
															flag=false;
													}
											}
											if(flag){
												boccupied[7][1] = ypos;
												check = false;
												isCheck();
												if(check){
													boccupied[7][1] = selYpos;
													check = false;
													flag = false;
													isCheck();
												}

												else if(b != -1)
													woccupied[b][0] = -1;
											}
										}
										else
											flag = false;
									}
								}

								/* REI PRETO */

								else if(selXpos == boccupied[6][0] && selYpos == boccupied[6][1]){
									if(
											(xpos == boccupied[6][0] + 1 || xpos == boccupied[6][0] - 1 || xpos == boccupied[6][0]) && 
											(ypos == boccupied[6][1] + 1 || ypos == boccupied[6][1] - 1 || ypos == boccupied[6][1])){


										boccupied[6][0] = xpos;
										boccupied[6][1] = ypos;
										check = false;
										isCheck();
										if(check){
											boccupied[6][0] = selXpos;
											boccupied[6][1] = selYpos;
											check = false;
											flag = false;
											isCheck();
										}

										else if(b != -1)
											woccupied[b][0] = -1;
									}

									else
										flag = false;
								}

								/* PE�ES PRETOS */

								else{
									for (int g = 0; g <= 7; g++) {
										if(selXpos == boccupied[g+8][0] && selYpos == boccupied[g+8][1]){
											if((boccupied[g+8][1] == 6 && xpos == boccupied[g+8][0] && ypos == boccupied[g+8][1] - 2) || (xpos == boccupied[g+8][0] && ypos == boccupied[g+8][1] - 1 && ypos >= 0)){
												boolean flag2 = true;
												for (int l = 0; l < boccupied.length; l++) {
													if(boccupied[l][0] == xpos && boccupied[l][1] == ypos || woccupied[l][0] == xpos && woccupied[l][1] == ypos ||
															boccupied[g+8][1] - ypos == 2 && boccupied[l][0] == xpos && boccupied[l][1] == boccupied[g+8][1]- 1 || boccupied[g+8][1] - ypos == 2 && woccupied[l][0] == xpos && woccupied[l][1] == boccupied[g+8][1]-100)
														flag2=false;
												}

												if(flag2){
													boccupied[g+8][1] = ypos;
													check = false;
													isCheck();
													if(check){
														boccupied[g+8][1] = selYpos;
														check = false;
														flag = false;
														isCheck();
													}

												}
												else
													flag = false;
											}

											else if((xpos == boccupied[g+8][0] + 1 || xpos == boccupied[g+8][0] - 1) && ypos == boccupied[g+8][1] - 1 && ypos >= 0 && b != -1){
												boccupied[g+8][1] = ypos;
												boccupied[g+8][0] = xpos;
												check = false;
												isCheck();
												if(check){
													boccupied[g+8][0] = selXpos;
													boccupied[g+8][1] = selYpos;
													check = false;
													flag = false;
													isCheck();
												}

												else
													woccupied[b][0] = -1;

											}
											else	
												flag = false;
										}
									}
								}

								xpos = 0;
								ypos = 0;

								if(flag){
									check = false;
									check2 = false;
									System.out.println("sent");
									Client.requestQueue.add(new RequestMove(id,Client.user, boccupied, woccupied));
									selected = false;
									isTurn = false;
								}
							}

							else if(!flag){
								selXpos = xpos;
								selYpos = ypos;
								selected = true;
							}
						}

						clicked = false;
					}	
				}
			}
		}

	}

	public boolean isCheckMate() {
		boolean flag = true;
		boolean doublecheck = check2;
		int[] checker = new int[2];
		int checkerindex = checkPieceIndex;
		int auxX, auxY = 0;
		boolean occflag = false;
		int occindex = 0;

		checker[0] = checkPiece[0];
		checker[1] = checkPiece[1];

		if(color == 1){
			for (int i = 0; i < boccupied.length; i++) {
				if(woccupied[i][0] == woccupied[6][0] + 1 && woccupied[i][1] == woccupied[6][1] || woccupied[6][0] == 7)
					flag = false;
				else if(boccupied[i][0] == woccupied[6][0] + 1 && boccupied[i][1] == woccupied[6][1]){
					occflag = true;
					occindex = i;
				}
			}

			if (flag) {
				if(occflag){
					boccupied[occindex][0] = -1;
					boccupied[occindex][1] = -1;
				}

				woccupied[6][0]++;
				check = false;
				isCheck();

				if(occflag){
					boccupied[occindex][0] = woccupied[6][0];
					boccupied[occindex][1] = woccupied[6][1];
				}

				woccupied[6][0] --;

				if(!check)
					return false;
				isCheck();
			}
			System.out.println("ola");
			flag = true;

			for (int i = 0; i < boccupied.length; i++) {
				if(woccupied[i][0] == woccupied[6][0] - 1 && woccupied[i][1] == woccupied[6][1]  || woccupied[6][0] == 0)
					flag = false;

				else if(boccupied[i][0] == woccupied[6][0] - 1 && boccupied[i][1] == woccupied[6][1] ){
					occflag = true;
					occindex = i;
				}
			}

			if (flag) {

				if(occflag){
					boccupied[occindex][0] = -1;
					boccupied[occindex][1] = -1;
				}

				woccupied[6][0]--;
				check = false;
				isCheck();

				if(occflag){
					boccupied[occindex][0] = woccupied[6][0];
					boccupied[occindex][1] = woccupied[6][1];
				}
				woccupied[6][0]++;

				if(!check)
					return false;
				isCheck();
			}
			
			flag = true;
			
			for (int i = 0; i < boccupied.length; i++) {
				if(woccupied[i][0] == woccupied[6][0] && woccupied[i][1] == woccupied[6][1] +1 || woccupied[i][1] == 7 )
					flag = false;

				else if(boccupied[i][0] == woccupied[6][0] && boccupied[i][1] == woccupied[6][1] + 1){
					occflag = true;
					occindex = i;
				}
			}
			
			if (flag) {
				if(occflag){
					boccupied[occindex][0] = -1;
					boccupied[occindex][1] = -1;
				}


				woccupied[6][1]++;
				check = false;
				isCheck();

				if(occflag){
					boccupied[occindex][0] = woccupied[6][0];
					boccupied[occindex][1] = woccupied[6][1];
				}
				woccupied[6][1]--;

				if(!check)
					return false;
				isCheck();
			}
			
			flag = true;
			for (int i = 0; i < boccupied.length; i++) {
				if(woccupied[i][0] == woccupied[6][0] && woccupied[i][1] == woccupied[6][1] - 1  || woccupied[6][0] == 0)
					flag = false;
				else if(boccupied[i][0] == woccupied[6][0] && boccupied[i][1] == woccupied[6][1] - 1){
					occflag = true;
					occindex = i;
				}
			}

			if (flag) {
				if(occflag){
					boccupied[occindex][0] = -1;
					boccupied[occindex][1] = -1;
				}

				woccupied[6][1]--;
				check = false;
				isCheck();

				if(occflag){
					boccupied[occindex][0] = woccupied[6][0];
					boccupied[occindex][1] = woccupied[6][1];
				}

				woccupied[6][1]++;

				if(!check)
					return false;
				isCheck();
			}
			
			flag = true;
			for (int i = 0; i < boccupied.length; i++) {
				if(woccupied[i][0] == woccupied[6][0] + 1 && woccupied[i][1] == woccupied[6][1] + 1  || woccupied[6][0] == 7 || woccupied[6][1] == 7 )
					flag = false;
				else if(boccupied[i][0] == woccupied[6][0] + 1 && boccupied[i][1] == woccupied[6][1] + 1){
					occflag = true;
					occindex = i;
				}
			}
			System.out.println("ola1");
			if (flag) {
				if(occflag){
					boccupied[occindex][0] = -1;
					boccupied[occindex][1] = -1;
				}

				woccupied[6][0]++;
				woccupied[6][1]++;
				check = false;
				isCheck();

				if(occflag){
					boccupied[occindex][0] = woccupied[6][0];
					boccupied[occindex][1] = woccupied[6][1];
				}
				woccupied[6][0]--;
				woccupied[6][1]--;

				if(!check)
					return false;
				isCheck();
			}
			flag = true;
			for (int i = 0; i < boccupied.length; i++) {
				if(woccupied[i][0] == woccupied[6][0] - 1 && woccupied[i][1] == woccupied[6][1] - 1  || woccupied[6][0] == 0 || woccupied[6][1] == 0)
					flag = false;
				else if(boccupied[i][0] == woccupied[6][0] - 1 && boccupied[i][1] == woccupied[6][1] - 1){
					occflag = true;
					occindex = i;
				}
			}
			System.out.println("ola2");
			if (flag) {
				if(occflag){
					boccupied[occindex][0] = -1;
					boccupied[occindex][1] = -1;
				}

				woccupied[6][0]--;
				woccupied[6][1]--;
				check = false;
				isCheck();

				if(occflag){
					boccupied[occindex][0] = woccupied[6][0];
					boccupied[occindex][1] = woccupied[6][1];
				}

				woccupied[6][0]++;
				woccupied[6][1]++;

				if(!check)
					return false;
				isCheck();
			}
			flag = true;
			for (int i = 0; i < boccupied.length; i++) {
				if(woccupied[i][0] == woccupied[6][0] + 1 && woccupied[i][1] == woccupied[6][1] - 1  || woccupied[6][0] == 7 || woccupied[6][1] == 0){
					flag = false;
				}
				else if(boccupied[i][0] == woccupied[6][0] + 1 && boccupied[i][1] == woccupied[6][1] - 1){
					occflag = true;
					occindex = i;
				}
			}
			
			if (flag) {
				if(occflag){
					boccupied[occindex][0] = -1;
					boccupied[occindex][1] = -1;
				}

				woccupied[6][0]++;
				woccupied[6][1]--;
				check = false;
				isCheck();

				if(occflag){
					boccupied[occindex][0] = woccupied[6][0];
					boccupied[occindex][1] = woccupied[6][1];
				}

				woccupied[6][0]--;
				woccupied[6][1]++;
				if(!check)
					return false;
				isCheck();
			}
			flag = true;
			for (int i = 0; i < boccupied.length; i++) {
				if(woccupied[i][0] == woccupied[6][0] - 1 && woccupied[i][1] == woccupied[6][1] + 1 || woccupied[6][0] == 0 || woccupied[6][1] == 7)
					flag = false;
				else if(boccupied[i][0] == woccupied[6][0] - 1 && boccupied[i][1] == woccupied[6][1] + 1){
					occflag = true;
					occindex = i;
				}
			}
			
			if (flag) {
				if(occflag){
					boccupied[occindex][0] = -1;
					boccupied[occindex][1] = -1;
				}

				woccupied[6][0]--;
				woccupied[6][1]++;
				check = false;
				isCheck();

				if(occflag){
					boccupied[occindex][0] = woccupied[6][0];
					boccupied[occindex][1] = woccupied[6][1];
				}

				woccupied[6][0]++;
				woccupied[6][1]--;

				if(!check)
					return false;
				isCheck();
			}
			flag = true;
			
			if(!doublecheck){  		// DOUBLE CHECK
				for (int j = 0; j < 2; j++) {
					if(checker[1] == woccupied[j][1] && checker[0]<=7){	
						for (int l = 0; l < woccupied.length; l++) {
							if(checker[0] < woccupied[j][0])
								for (int x = checker[0]+1; x < woccupied[j][0]; x++) {
									if(woccupied[l][0] == x && woccupied[l][1] == checker[1] || boccupied[l][0] == x && boccupied[l][1] == checker[1])
										flag=false;
								}
							else
								for (int x = checker[0]-1; x > woccupied[j][0]; x--) {
									if(woccupied[l][0] == x && woccupied[l][1] == checker[1] || boccupied[l][0] == x && boccupied[l][1] == checker[1])
										flag=false;
								}
						}

						if(flag){
							auxY = woccupied[j][1];
							auxX = woccupied[j][0];
							woccupied[j][0] = checker[0];
							woccupied[j][1] = checker[1];
							boccupied[checkerindex][0] = -1;
							boccupied[checkerindex][1] = -1;
							check = false;
							isCheck();

							woccupied[j][0] = auxX;
							woccupied[j][1] = auxY;
							boccupied[checkerindex][0] = checker[0];
							boccupied[checkerindex][1] = checker[1];

							if(!check)
								return false;
							isCheck();
						}
						flag = true;
					}

					else if(checker[0] == woccupied[j][0] && checker[1]<=7){  
						for (int l = 0; l < woccupied.length; l++) {
							if(checker[1] < woccupied[j][1])
								for (int y =  checker[1]+1; y < woccupied[j][1]; y++) {
									if(woccupied[l][0] == checker[0] && woccupied[l][1] == y || boccupied[l][0] == checker[0] && boccupied[l][1] == y)
										flag=false;
								}
							else
								for (int y =  checker[1]-1; y > woccupied[j][1]; y--) {
									if(woccupied[l][0] == checker[0] && woccupied[l][1] == y || boccupied[l][0] == checker[0] && boccupied[l][1] == y)
										flag=false;
								}
						}

						if(flag){
							auxY = woccupied[j][1];
							auxX = woccupied[j][0];
							woccupied[j][0] = checker[0];
							woccupied[j][1] = checker[1];
							boccupied[checkerindex][0] = -1;
							boccupied[checkerindex][1] = -1;
							check = false;
							isCheck();

							woccupied[j][0] = auxX;
							woccupied[j][1] = auxY;
							boccupied[checkerindex][0] = checker[0];
							boccupied[checkerindex][1] = checker[1];

							if(!check)
								return false;
							isCheck();
						}
						flag = true;
					}

					else
						flag = false;

					if(checker[1] == woccupied[j+2][1]-2 && checker[0] == woccupied[j+2][0]+1
							|| checker[1] == woccupied[j+2][1]-2 && checker[0] == woccupied[j+2][0]-1
							|| checker[1] == woccupied[j+2][1]+2 && checker[0] == woccupied[j+2][0]-1
							|| checker[1] == woccupied[j+2][1]+2 && checker[0] == woccupied[j+2][0]+1
							|| checker[1] == woccupied[j+2][1]-1 && checker[0] == woccupied[j+2][0]+2
							|| checker[1] == woccupied[j+2][1]-1 && checker[0] == woccupied[j+2][0]-2
							|| checker[1] == woccupied[j+2][1]+1 && checker[0] == woccupied[j+2][0]-2
							|| checker[1] == woccupied[j+2][1]+1 && checker[0] == woccupied[j+2][0]+2){

						auxY = woccupied[j+2][1];
						auxX = woccupied[j+2][0];
						woccupied[j+2][0] = checker[0];
						woccupied[j+2][1] = checker[1];
						boccupied[checkerindex][0] = -1;
						boccupied[checkerindex][1] = -1;
						check = false;
						isCheck();

						woccupied[j+2][0] = auxX;
						woccupied[j+2][1] = auxY;
						boccupied[checkerindex][0] = checker[0];
						boccupied[checkerindex][1] = checker[1];

						if(!check)
							return false;
						isCheck();
					}
					flag = true;

					for (int g = 0; g <= 7; g ++) {  // BISPOS
						if(
								(checker[0] == woccupied[j+4][0] + g && checker[1]== woccupied[j+4][1] + g)||
								(checker[0] == woccupied[j+4][0] - g && checker[1]== woccupied[j+4][1] - g)||
								(checker[0] == woccupied[j+4][0] + g && checker[1]== woccupied[j+4][1] - g)||
								(checker[0] == woccupied[j+4][0] - g && checker[1]== woccupied[j+4][1] + g)){

							for (int l = 0; l < boccupied.length; l++) {
								if(checker[0] < woccupied[j+4][0] && checker[1] < woccupied[j+4][1])
									for (int x = checker[0]+1, y = checker[1]+1; x < woccupied[j+4][0]; x++, y++) {
										if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
											flag=false;
									}
								else if(checker[0] < woccupied[j+4][0] && checker[1] > woccupied[j+4][1])
									for (int x = checker[0]+1, y = checker[1]-1; x < woccupied[j+4][0]; x++, y--) {
										if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
											flag=false;
									}
								else if(checker[0] > woccupied[j+4][0] && checker[1] < woccupied[j+4][1])
									for (int x = checker[0]-1, y = checker[1]+1; x > woccupied[j+4][0]; x--, y++) {
										if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
											flag=false;
									}
								else if(checker[0] > woccupied[j+4][0] && checker[1] > woccupied[j+4][1])
									for (int x = checker[0]-1, y = checker[1]-1; x > woccupied[j+4][0]; x--, y--) {
										if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
											flag=false;

									}
							}

							if(flag){
								auxY = woccupied[j+4][1];
								auxX = woccupied[j+4][0];
								woccupied[j+4][0] = checker[0];
								woccupied[j+4][1] = checker[1];
								boccupied[checkerindex][0] = -1;
								boccupied[checkerindex][1] = -1;
								check = false;
								isCheck();

								woccupied[j+4][0] = auxX;
								woccupied[j+4][1] = auxY;
								boccupied[checkerindex][0] = checker[0];
								boccupied[checkerindex][1] = checker[1];

								if(!check)
									return false;
								isCheck();
							}
							flag = true;
						}
					}
				}

				if(checker[0] != woccupied[7][0] && checker[1] != woccupied[7][1]){ // RAINHA
					for (int g = 0; g <= 7; g++) {
						if(
								(checker[0] == woccupied[7][0] + g && checker[1] == woccupied[7][1] + g)||
								(checker[0] == woccupied[7][0] - g && checker[1] == woccupied[7][1] - g)||
								(checker[0] == woccupied[7][0] + g && checker[1] == woccupied[7][1] - g)||
								(checker[0] == woccupied[7][0] - g && checker[1] == woccupied[7][1] + g)){


							for (int l = 0; l < woccupied.length; l++) {
								if(checker[0] < woccupied[7][0] && checker[1] < woccupied[7][1])
									for (int x = checker[0]+1, y = checker[1]+1; x < woccupied[7][0]; x++, y++) {
										if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
											flag=false;
									}
								else if(checker[0] < woccupied[7][0] && checker[1] > woccupied[7][1])
									for (int x = checker[0]+1, y = checker[1]-1; x < woccupied[7][0]; x++, y--) {
										if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
											flag=false;
									}
								else if(checker[0] > woccupied[7][0] && checker[1] < woccupied[7][1])
									for (int x = checker[0]-1, y = checker[1]+1; x > woccupied[7][0]; x--,y++) {
										if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
											flag=false;
									}
								else if(checker[0] > woccupied[7][0] && checker[1] > woccupied[7][1])
									for (int x = checker[0]-1, y = checker[1]-1; x > woccupied[7][0]; x--, y--) {
										if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
											flag=false;
									}
							}

							if(flag){
								auxY = woccupied[7][1];
								auxX = woccupied[7][0];
								woccupied[7][0] = checker[0];
								woccupied[7][1] = checker[1];
								boccupied[checkerindex][0] = -1;
								boccupied[checkerindex][1] = -1;
								check = false;
								isCheck();

								woccupied[7][0] = auxX;
								woccupied[7][1] = auxY;
								boccupied[checkerindex][0] = checker[0];
								boccupied[checkerindex][1] = checker[1];

								if(!check)
									return false;	
								isCheck();
							}
							flag = true;
						}	
					}
				}

				else{
					if(checker[1] == woccupied[7][1] && checker[0]<=7){
						for (int l = 0; l < woccupied.length; l++) {
							if(checker[0] < woccupied[7][0])
								for (int x = checker[0] + 1; x < woccupied[7][0]; x++) {
									if(woccupied[l][0] == x && woccupied[l][1] == checker[1] || boccupied[l][0] == x && boccupied[l][1] == checker[1])
										flag=false;
								}
							else
								for (int x = checker[0] - 1 ; x > woccupied[7][0]; x--) {
									if(woccupied[l][0] == x && woccupied[l][1] == checker[1] || boccupied[l][0] == x && boccupied[l][1] == checker[1])
										flag=false;
								}
						}

						if(flag){
							auxY = woccupied[7][1];
							auxX = woccupied[7][0];
							woccupied[7][0] = checker[0];
							woccupied[7][1] = checker[1];
							boccupied[checkerindex][0] = -1;
							boccupied[checkerindex][1] = -1;
							check = false;
							isCheck();

							woccupied[7][0] = auxX;
							woccupied[7][1] = auxY;
							boccupied[checkerindex][0] = checker[0];
							boccupied[checkerindex][1] = checker[1];

							if(!check)
								return false;	
							isCheck();
						}
						flag = true;
					}

					else if(checker[0] == woccupied[7][0] && checker[1]<=7){
						for (int l = 0; l < woccupied.length; l++) {
							if(checker[1] < woccupied[7][1]){
								for (int y = checker[1] +1 ; y < woccupied[7][1]; y++) {
									if(woccupied[l][0] == checker[0]  && woccupied[l][1] == y || boccupied[l][0] == checker[0]  && boccupied[l][1] == y)
										flag=false;
								}
							}
							else{
								for (int y = checker[1] - 1; y > woccupied[7][1]; y--) {
									if(woccupied[l][0] == checker[0]  && woccupied[l][1] == y || boccupied[l][0] == checker[0]  && boccupied[l][1] == y)
										flag=false;
								}
							}
						}

						if(flag){
							auxY = woccupied[7][1];
							auxX = woccupied[7][0];
							woccupied[7][0] = checker[0];
							woccupied[7][1] = checker[1];
							boccupied[checkerindex][0] = -1;
							boccupied[checkerindex][1] = -1;
							check = false;
							isCheck();

							woccupied[7][0] = auxX;
							woccupied[7][1] = auxY;
							boccupied[checkerindex][0] = checker[0];
							boccupied[checkerindex][1] = checker[1];

							if(!check)
								return false;	
							isCheck();
						}
						flag = true;
					}
				}

				for (int g = 0; g <= 7; g++) {
					if((checker[0] == woccupied[g+8][0] + 1 || checker[0] == woccupied[g+8][0] - 1) && checker[1] == woccupied[g+8][1] + 1 && checker[1] >= 0){
						auxY = woccupied[g+8][1];
						auxX = woccupied[g+8][0];
						woccupied[g+8][0] = checker[0];
						woccupied[g+8][1] = checker[1];
						boccupied[checkerindex][0] = -1;
						boccupied[checkerindex][1] = -1;
						check = false;
						isCheck();

						woccupied[g+8][0] = auxX;
						woccupied[g+8][1] = auxY;
						boccupied[checkerindex][0] = checker[0];
						boccupied[checkerindex][1] = checker[1];

						if(!check)
							return false;
						isCheck();

					}
					flag = true;
				}

			}
		}

		else{
			return false;
		}
		return true;
	}
	public void isCheck() {    //IS CHECK
		if(color == 1){
			boolean flagcheck = true;

			for (int j = 0; j < 2; j++) {
				flagcheck = true;
				if(boccupied[j][0] == woccupied[6][0]){
					if(boccupied[j][1] == woccupied[6][1] + 1 || boccupied[j][1] == woccupied[6][1] - 1){
						if(check)
							check2 = true;
						check = true;
						checkPieceIndex = j;
						checkPiece[0] = boccupied[j][0];
						checkPiece[1] = boccupied[j][1];

					}

					else{
						for (int y = 1; y <= Math.abs(boccupied[j][1] - woccupied[6][1]); y++) {
							for (int k = 0; k < boccupied.length; k++) {
								if (woccupied[6][1] > boccupied[j][1]) {
									if(k != j && k != 6 && (boccupied[k][0] == woccupied[6][0] && boccupied[k][1] == woccupied[6][1] - y || woccupied[k][0] == woccupied[6][0] && woccupied[k][1] == woccupied[6][1] - y)){
										flagcheck = false;
										y = 7;
									}
								}

								else{
									if(k != j && k != 6 && ( boccupied[k][0] == woccupied[6][0] && boccupied[k][1] == woccupied[6][1] + y ||  woccupied[k][0] == woccupied[6][0] && woccupied[k][1] == woccupied[6][1] + y)){
										flagcheck = false;
										y = 7;
									}
								}
							}
						}
					}

					if(flagcheck){
						if(check)
							check2 = true;
						check = true;
						checkPieceIndex = j;
						checkPiece[0] = boccupied[j][0];
						checkPiece[1] = boccupied[j][1];

					}
				}

				else if(boccupied[j][1] == woccupied[6][1]){
					if(boccupied[j][0] == woccupied[6][0] + 1 || boccupied[j][0] == woccupied[6][0] - 1){
						if(check)
							check2 = true;
						check = true;
						checkPieceIndex = j;
						checkPiece[0] = boccupied[j][0];
						checkPiece[1] = boccupied[j][1];

					}

					else{
						for (int y = 1; y < Math.abs(boccupied[j][0] - woccupied[6][0]); y++) {
							for (int k = 0; k < boccupied.length; k++) {
								if (woccupied[6][0] > boccupied[j][0]) {
									if(k != j && k != 6 && (boccupied[k][1] == woccupied[6][1] && boccupied[k][0] == woccupied[6][0] - y || woccupied[k][1] == woccupied[6][1] && woccupied[k][0] == woccupied[6][0] - y )){
										flagcheck = false;
										y = 7;
									}
								}

								else{
									if(k != j && k != 6 && (boccupied[k][1] == woccupied[6][1] && boccupied[k][0] == woccupied[6][0] + y || woccupied[k][1] == woccupied[6][1] && woccupied[k][0] == woccupied[6][0] + y )){
										flagcheck = false;
										y = 7;
									}
								}
							}
						}
					}

					if(flagcheck){
						if(check)
							check2 = true;
						check = true;
						checkPieceIndex = j;
						checkPiece[0] = boccupied[j][0];
						checkPiece[1] = boccupied[j][1];

					}
				}

				if(woccupied[6][1] == boccupied[j+2][1]-2 && woccupied[6][0] == boccupied[j+2][0]+1
						|| woccupied[6][1] == boccupied[j+2][1]-2 && woccupied[6][0] == boccupied[j+2][0]-1
						|| woccupied[6][1] == boccupied[j+2][1]+2 && woccupied[6][0] == boccupied[j+2][0]-1
						|| woccupied[6][1] == boccupied[j+2][1]+2 && woccupied[6][0] == boccupied[j+2][0]+1
						|| woccupied[6][1] == boccupied[j+2][1]-1 && woccupied[6][0] == boccupied[j+2][0]+2
						|| woccupied[6][1] == boccupied[j+2][1]-1 && woccupied[6][0] == boccupied[j+2][0]-2
						|| woccupied[6][1] == boccupied[j+2][1]+1 && woccupied[6][0] == boccupied[j+2][0]-2
						|| woccupied[6][1] == boccupied[j+2][1]+1 && woccupied[6][0] == boccupied[j+2][0]+2){
					if(check)
						check2 = true;
					check=true;
					checkPieceIndex = j+2;
					checkPiece[0] = boccupied[j+2][0];
					checkPiece[1] = boccupied[j+2][1];

				}

				for (int g = 0; g <= 7; g ++) {
					if(
							(woccupied[6][0] == boccupied[j+4][0] + g && woccupied[6][1]== boccupied[j+4][1] + g)||
							(woccupied[6][0] == boccupied[j+4][0] - g && woccupied[6][1]== boccupied[j+4][1] - g)||
							(woccupied[6][0] == boccupied[j+4][0] + g && woccupied[6][1]== boccupied[j+4][1] - g)||
							(woccupied[6][0] == boccupied[j+4][0] - g && woccupied[6][1]== boccupied[j+4][1] + g)){

						for (int l = 0; l < boccupied.length; l++) {
							if(woccupied[6][0] < boccupied[j+4][0] && woccupied[6][1] < boccupied[j+4][1])
								for (int x = woccupied[6][0]+1, y = woccupied[6][1]+1; x < boccupied[j+4][0]; x++, y++) {
									if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
										flagcheck=false;
								}
							else if(woccupied[6][0] < boccupied[j+4][0] && woccupied[6][1] > boccupied[j+4][1])
								for (int x = woccupied[6][0]+1, y = woccupied[6][1]-1; x < boccupied[j+4][0]; x++, y--) {
									if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
										flagcheck=false;
								}
							else if(woccupied[6][0] > boccupied[j+4][0] && woccupied[6][1] < boccupied[j+4][1])
								for (int x = woccupied[6][0]-1, y = woccupied[6][1]+1; x > boccupied[j+4][0]; x--, y++) {
									if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
										flagcheck=false;
								}
							else if(woccupied[6][0] > boccupied[j+4][0] && woccupied[6][1] > boccupied[j+4][1])
								for (int x = woccupied[6][0]-1, y = woccupied[6][1]-1; x > boccupied[j+4][0]; x--, y--) {
									if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
										flagcheck=false;

								}
						}

						if (flagcheck){
							if(check)
								check2 = true;
							check = true;
							checkPieceIndex = j+4;
							checkPiece[0] = boccupied[j+4][0];
							checkPiece[1] = boccupied[j+4][1];

						}
					}
				}
			}

			flagcheck = true;

			if(woccupied[6][0] != boccupied[7][0] && woccupied[6][1] != boccupied[7][1]){
				for (int g = 0; g <= 7; g++) {
					if(
							(woccupied[6][0] == boccupied[7][0] + g && woccupied[6][1] == boccupied[7][1] + g)||
							(woccupied[6][0] == boccupied[7][0] - g && woccupied[6][1] == boccupied[7][1] - g)||
							(woccupied[6][0] == boccupied[7][0] + g && woccupied[6][1] == boccupied[7][1] - g)||
							(woccupied[6][0] == boccupied[7][0] - g && woccupied[6][1] == boccupied[7][1] + g)){


						for (int l = 0; l < woccupied.length; l++) {
							if(woccupied[6][0] < boccupied[7][0] && woccupied[6][1] < boccupied[7][1])
								for (int x = woccupied[6][0]+1, y = woccupied[6][1]+1; x < boccupied[7][0]; x++, y++) {
									if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
										flagcheck=false;
								}
							else if(woccupied[6][0] < boccupied[7][0] && woccupied[6][1] > boccupied[7][1])
								for (int x = woccupied[6][0]+1, y = woccupied[6][1]-1; x < boccupied[7][0]; x++, y--) {
									if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
										flagcheck=false;
								}
							else if(woccupied[6][0] > boccupied[7][0] && woccupied[6][1] < boccupied[7][1])
								for (int x = woccupied[6][0]-1, y = woccupied[6][1]+1; x > boccupied[7][0]; x--,y++) {
									if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
										flagcheck=false;
								}
							else if(woccupied[6][0] > boccupied[7][0] && woccupied[6][1] > boccupied[7][1])
								for (int x = woccupied[6][0]-1, y = woccupied[6][1]-1; x > boccupied[7][0]; x--, y--) {
									if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
										flagcheck=false;
								}
						}

						if(flagcheck){
							if(check)
								check2 = true;
							check = true;
							checkPieceIndex = 7;
							checkPiece[0] = boccupied[7][0];
							checkPiece[1] = boccupied[7][1];

						}
					}	
				}
			}

			else{
				if(woccupied[6][1] == boccupied[7][1] && (woccupied[6][0] < boccupied[7][0] || woccupied[6][0] > boccupied[7][0])){
					for (int l = 0; l < woccupied.length; l++) {
						if(woccupied[6][0] < boccupied[7][0])
							for (int x = woccupied[6][0] + 1; x < boccupied[7][0]; x++) {
								if(woccupied[l][0] == x && woccupied[l][1] == woccupied[6][1] || boccupied[l][0] == x && boccupied[l][1] == woccupied[6][1])
									flagcheck=false;
							}
						else
							for (int x = woccupied[6][0] - 1 ; x > woccupied[7][0]; x--) {
								if(woccupied[l][0] == x && woccupied[l][1] == woccupied[6][1] || boccupied[l][0] == x && boccupied[l][1] == woccupied[6][1])
									flagcheck=false;
							}
					}

					if(flagcheck){
						if(check)
							check2 = true;
						check = true;
						checkPieceIndex = 7;
						checkPiece[0] = boccupied[7][0];
						checkPiece[1] = boccupied[7][1];

					}
				}

				else if(woccupied[6][0] == boccupied[7][0] && (woccupied[6][1] < boccupied[7][1] || woccupied[6][1] > boccupied[7][1])){
					for (int l = 0; l < woccupied.length; l++) {
						if(woccupied[6][1] < boccupied[7][1]){
							for (int y = woccupied[6][1] +1 ; y < boccupied[7][1]; y++) {
								if(woccupied[l][0] == woccupied[6][0]  && woccupied[l][1] == y || boccupied[l][0] == woccupied[6][0]  && boccupied[l][1] == y)
									flagcheck=false;
							}
						}
						else{
							for (int y = woccupied[6][1] - 1; y > boccupied[7][1]; y--) {
								if(woccupied[l][0] == woccupied[6][0]  && woccupied[l][1] == y || boccupied[l][0] == woccupied[6][0]  && boccupied[l][1] == y)
									flagcheck=false;
							}
						}
					}

					if(flagcheck){
						if(check)
							check2 = true;
						check = true;
						checkPieceIndex = 7;
						checkPiece[0] = boccupied[7][0];
						checkPiece[1] = boccupied[7][1];

					}
				}
			}

			for (int g = 0; g <= 7; g++) {
				if((woccupied[6][0] == boccupied[g+8][0] + 1 || woccupied[6][0] == boccupied[g+8][0] - 1) && woccupied[6][1] == boccupied[g+8][1] + 1 && woccupied[6][1] >= 0){
					if(check)
						check2 = true;
					check = true;
					checkPieceIndex = g+8;
					checkPiece[0] = boccupied[g+8][0];
					checkPiece[1] = boccupied[g+8][1];

				}
			}

		}


		else{
			boolean flagcheck = true;

			for (int j = 0; j < 2; j++) {
				flagcheck = true;
				if(woccupied[j][0] == boccupied[6][0]){
					if(woccupied[j][1] == boccupied[6][1] + 1 || woccupied[j][1] == boccupied[6][1] - 1){
						if(check)
							check2 = true;
						check = true;
						checkPieceIndex = j;
						checkPiece[0] = boccupied[j][0];
						checkPiece[1] = boccupied[j][1];

					}

					else{
						for (int y = 1; y <= Math.abs(woccupied[j][1] - boccupied[6][1]); y++) {
							for (int k = 0; k < woccupied.length; k++) {
								if (boccupied[6][1] > woccupied[j][1]) {
									if(k != j && k != 6 && (woccupied[k][0] == boccupied[6][0] && woccupied[k][1] == boccupied[6][1] - y || boccupied[k][0] == boccupied[6][0] && boccupied[k][1] == boccupied[6][1] - y)){
										flagcheck = false;
										y = 7;
									}
								}

								else{
									if(k != j && k != 6 && ( woccupied[k][0] == boccupied[6][0] && woccupied[k][1] == boccupied[6][1] + y ||  boccupied[k][0] == boccupied[6][0] && boccupied[k][1] == boccupied[6][1] + y)){
										flagcheck = false;
										y = 7;
									}
								}
							}
						}
					}

					if(flagcheck){
						if(check)
							check2 = true;
						check = true;
						checkPieceIndex = j;
						checkPiece[0] = boccupied[j][0];
						checkPiece[1] = boccupied[j][1];

					}
				}

				else if(woccupied[j][1] == boccupied[6][1]){
					if(woccupied[j][0] == boccupied[6][0] + 1 || woccupied[j][0] == boccupied[6][0] - 1){
						if(check)
							check2 = true;
						check = true;
						checkPieceIndex = j;
						checkPiece[0] = boccupied[j][0];
						checkPiece[1] = boccupied[j][1];

					}

					else{
						for (int y = 1; y < Math.abs(woccupied[j][0] - boccupied[6][0]); y++) {
							for (int k = 0; k < boccupied.length; k++) {
								if (boccupied[6][0] > woccupied[j][0]) {
									if(k != j && k != 6 && (boccupied[k][1] == boccupied[6][1] && boccupied[k][0] == boccupied[6][0] - y || woccupied[k][1] == boccupied[6][1] && woccupied[k][0] == boccupied[6][0] - y )){
										flagcheck = false;
										y = 7;
									}
								}

								else{
									if(k != j && k != 6 && (boccupied[k][1] == boccupied[6][1] && boccupied[k][0] == boccupied[6][0] + y || woccupied[k][1] == boccupied[6][1] && woccupied[k][0] == boccupied[6][0] + y )){
										flagcheck = false;
										y = 7;
									}
								}
							}
						}
					}

					if(flagcheck){
						if(check)
							check2 = true;
						check = true;
						checkPieceIndex = j;
						checkPiece[0] = boccupied[j][0];
						checkPiece[1] = boccupied[j][1];

					}
				}

				if(boccupied[6][1] == woccupied[j+2][1]-2 && boccupied[6][0] == woccupied[j+2][0]+1
						|| boccupied[6][1] == woccupied[j+2][1]-2 && boccupied[6][0] == woccupied[j+2][0]-1
						|| boccupied[6][1] == woccupied[j+2][1]+2 && boccupied[6][0] == woccupied[j+2][0]-1
						|| boccupied[6][1] == woccupied[j+2][1]+2 && boccupied[6][0] == woccupied[j+2][0]+1
						|| boccupied[6][1] == woccupied[j+2][1]-1 && boccupied[6][0] == woccupied[j+2][0]+2
						|| boccupied[6][1] == woccupied[j+2][1]-1 && boccupied[6][0] == woccupied[j+2][0]-2
						|| boccupied[6][1] == woccupied[j+2][1]+1 && boccupied[6][0] == woccupied[j+2][0]-2
						|| boccupied[6][1] == woccupied[j+2][1]+1 && boccupied[6][0] == woccupied[j+2][0]+2){
					if(check)
						check2 = true;
					check=true;
					checkPieceIndex = j+2;
					checkPiece[0] = boccupied[j+2][0];
					checkPiece[1] = boccupied[j+2][1];

				}

				for (int g = 0; g <= 7; g ++) {
					if(
							(boccupied[6][0] == woccupied[j+4][0] + g && boccupied[6][1]== woccupied[j+4][1] + g)||
							(boccupied[6][0] == woccupied[j+4][0] - g && boccupied[6][1]== woccupied[j+4][1] - g)||
							(boccupied[6][0] == woccupied[j+4][0] + g && boccupied[6][1]== woccupied[j+4][1] - g)||
							(boccupied[6][0] == woccupied[j+4][0] - g && boccupied[6][1]== woccupied[j+4][1] + g)){

						for (int l = 0; l < boccupied.length; l++) {
							if(boccupied[6][0] < woccupied[j+4][0] && boccupied[6][1] < woccupied[j+4][1])
								for (int x = boccupied[6][0]+1, y = boccupied[6][1]+1; x < woccupied[j+4][0]; x++, y++) {
									if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
										flagcheck=false;
								}
							else if(boccupied[6][0] < woccupied[j+4][0] && boccupied[6][1] > woccupied[j+4][1])
								for (int x = boccupied[6][0]+1, y = boccupied[6][1]-1; x < woccupied[j+4][0]; x++, y--) {
									if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
										flagcheck=false;
								}
							else if(boccupied[6][0] > woccupied[j+4][0] && boccupied[6][1] < woccupied[j+4][1])
								for (int x = boccupied[6][0]-1, y = boccupied[6][1]+1; x > woccupied[j+4][0]; x--, y++) {
									if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
										flagcheck=false;
								}
							else if(boccupied[6][0] > woccupied[j+4][0] && boccupied[6][1] > woccupied[j+4][1])
								for (int x = boccupied[6][0]-1, y = boccupied[6][1]-1; x > woccupied[j+4][0]; x--, y--) {
									if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
										flagcheck=false;

								}
						}

						if (flagcheck){
							if(check)
								check2 = true;
							check = true;
							checkPieceIndex = j+4;
							checkPiece[0] = boccupied[j+4][0];
							checkPiece[1] = boccupied[j+4][1];

						}
					}
				}
			}

			flagcheck = true;

			if(boccupied[6][0] != woccupied[7][0] && boccupied[6][1] != woccupied[7][1]){
				for (int g = 0; g <= 7; g++) {
					if(
							(boccupied[6][0] == woccupied[7][0] + g && boccupied[6][1] == woccupied[7][1] + g)||
							(boccupied[6][0] == woccupied[7][0] - g && boccupied[6][1] == woccupied[7][1] - g)||
							(boccupied[6][0] == woccupied[7][0] + g && boccupied[6][1] == woccupied[7][1] - g)||
							(boccupied[6][0] == woccupied[7][0] - g && boccupied[6][1] == woccupied[7][1] + g)){


						for (int l = 0; l < woccupied.length; l++) {
							if(boccupied[6][0] < woccupied[7][0] && boccupied[6][1] < woccupied[7][1])
								for (int x = boccupied[6][0]+1, y = boccupied[6][1]+1; x < woccupied[7][0]; x++, y++) {
									if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
										flagcheck=false;
								}
							else if(boccupied[6][0] < woccupied[7][0] && boccupied[6][1] > woccupied[7][1])
								for (int x = boccupied[6][0]+1, y = boccupied[6][1]-1; x < woccupied[7][0]; x++, y--) {
									if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
										flagcheck=false;
								}
							else if(boccupied[6][0] > woccupied[7][0] && boccupied[6][1] < woccupied[7][1])
								for (int x = boccupied[6][0]-1, y = boccupied[6][1]+1; x > woccupied[7][0]; x--,y++) {
									if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
										flagcheck=false;
								}
							else if(boccupied[6][0] > woccupied[7][0] && boccupied[6][1] > woccupied[7][1])
								for (int x = boccupied[6][0]-1, y = boccupied[6][1]-1; x > woccupied[7][0]; x--, y--) {
									if(woccupied[l][0] == x && woccupied[l][1] == y || boccupied[l][0] == x && boccupied[l][1] == y)
										flagcheck=false;
								}
						}

						if(flagcheck){
							if(check)
								check2 = true;
							check = true;
							checkPieceIndex = 7;
							checkPiece[0] = boccupied[7][0];
							checkPiece[1] = boccupied[7][1];

						}
					}	
				}
			}

			else{
				if(boccupied[6][1] == woccupied[7][1] && boccupied[6][0]<=7){
					for (int l = 0; l < woccupied.length; l++) {
						if(boccupied[6][0] < woccupied[7][0])
							for (int x = boccupied[6][0] + 1; x < woccupied[7][0]; x++) {
								if(woccupied[l][0] == x && woccupied[l][1] == boccupied[6][1] || boccupied[l][0] == x && boccupied[l][1] == boccupied[6][1])
									flagcheck=false;
							}
						else
							for (int x = boccupied[6][0] - 1 ; x > woccupied[7][0]; x--) {
								if(woccupied[l][0] == x && woccupied[l][1] == boccupied[6][1] || boccupied[l][0] == x && boccupied[l][1] == boccupied[6][1])
									flagcheck=false;
							}
					}

					if(flagcheck){
						if(check)
							check2 = true;
						check = true;
						checkPieceIndex = 7;
						checkPiece[0] = boccupied[7][0];
						checkPiece[1] = boccupied[7][1];

					}
				}

				else if(boccupied[6][0] == woccupied[7][0] && boccupied[6][1]<=7){
					for (int l = 0; l < woccupied.length; l++) {
						if(boccupied[6][1] < woccupied[7][1]){
							for (int y = boccupied[6][1] +1 ; y < woccupied[7][1]; y++) {
								if(woccupied[l][0] == boccupied[6][0]  && woccupied[l][1] == y || boccupied[l][0] == boccupied[6][0]  && boccupied[l][1] == y)
									flagcheck=false;
							}
						}
						else{
							for (int y = boccupied[6][1] - 1; y > woccupied[7][1]; y--) {
								if(woccupied[l][0] == boccupied[6][0]  && woccupied[l][1] == y || boccupied[l][0] == boccupied[6][0]  && boccupied[l][1] == y)
									flagcheck=false;
							}
						}
					}

					if(flagcheck){
						if(check)
							check2 = true;
						check = true;
						checkPieceIndex = 7;
						checkPiece[0] = boccupied[7][0];
						checkPiece[1] = boccupied[7][1];

					}
				}
			}

			for (int g = 0; g <= 7; g++) {
				if((boccupied[6][0] == woccupied[g+8][0] + 1 || boccupied[6][0] == woccupied[g+8][0] - 1) && boccupied[6][1] == woccupied[g+8][1] + 1 && boccupied[6][1] >= 0){
					if(check)
						check2 = true;
					check = true;
					checkPieceIndex = 7;
					checkPiece[0] = boccupied[g+8][0];
					checkPiece[1] = boccupied[g+8][1];

				}
			}

		}

	}


	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException
	{
		if(end){
			if(reason==1){
				g.drawImage(new Image("game/victorydisconnect.png"), 0, 0, resX, resY, 0, 0, 1100, 900);	

				if(continuePressed)
					g.drawImage(new Image("game/buttoncontinuein.png"), sclX(340), sclY(660), sclX(770), sclY(775), 0, 0, 430, 115);
				else
					g.drawImage(new Image("game/buttoncontinueout.png"), sclX(340), sclY(660), sclX(770), sclY(775), 0, 0, 430, 115);
			}

			else if(reason == 0){
				if(isWinner);

				else;
			}

			else if(reason == 2){
				if(isWinner);

				else;
			}

		}

		else if(isRPS){
			g.drawImage(new Image("game/rps.png"), 0, 0, resX, resY, 0, 0, 1100, 900);

			if(win!=-1){
				if(win == 0)
					g.drawImage(loserRps, 0 , 0, Math.round(loserRps.getWidth()*resX/1100), Math.round(loserRps.getHeight()*resY/900), 0,0,loserRps.getWidth(), loserRps.getHeight());
				else if(win == 1)
					g.drawImage(winnerRps, 0 , 0, Math.round(winnerRps.getWidth()*resX/1100), Math.round(winnerRps.getHeight()*resY/900), 0,0,loserRps.getWidth(), loserRps.getHeight());
				else;
			}

			switch (rps) {
			case 0: break;
			case 1:
				g.drawImage(rock, Math.round(146*resX/1100), Math.round(547*resY/900),Math.round(430*resX/1100), Math.round(836*resY/900) ,0,0, rock.getWidth(),rock.getHeight());
				break;
			case 2:
				g.drawImage(paper, Math.round(165*resX/1100), Math.round(555*resY/900),Math.round(430*resX/1100), Math.round(836*resY/900) ,0,0, paper.getWidth(),paper.getHeight());
				break;
			case 3:
				g.drawImage(scissors, Math.round(140*resX/1100), Math.round(540*resY/900),Math.round(430*resX/1100), Math.round(836*resY/900) ,0,0, scissors.getWidth(),scissors.getHeight());
				break;
			}

			switch (rpsOpp) {
			case 0: break;
			case 1:
				g.drawImage(rock, Math.round(661*resX/1100), Math.round(547*resY/900),Math.round(945*resX/1100), Math.round(836*resY/900),0,0, rock.getWidth(),rock.getHeight());
				break;
			case 2:
				g.drawImage(paper, Math.round(675*resX/1100), Math.round(555*resY/900),Math.round(945*resX/1100), Math.round(836*resY/900),0,0, paper.getWidth(), paper.getHeight());
				break;
			case 3:
				g.drawImage(scissors, Math.round(645*resX/1100), Math.round(540*resY/900),Math.round(945*resX/1100), Math.round(836*resY/900),0,0, scissors.getWidth(), scissors.getHeight());
				break;
			}
		}

		if((reason == -1 || reason == 2 || reason == 0) && !isRPS){
			if(!end)
				g.drawImage(new Image("game/backchess.png"), 0, 0, resX, resY, 0, 0, 1100, 900);
			else{
				if(isWinner)
					g.drawImage(new Image("game/victory.png"), 0, 0, resX, resY, 0, 0, 1100, 900);
				else
					g.drawImage(new Image("game/defeat.png"), 0, 0, resX, resY, 0, 0, 1100, 900);
				if(continuePressed)
					g.drawImage(new Image("game/continueEndin.png"), 810, 835, 1063, 893, 0, 0, 253, 58);
				else
					g.drawImage(new Image("game/continueEndout.png"), 802, 835, 1055, 893, 0, 0, 253, 58);

			}
			int scX = sclX(100);
			int scY = sclY(100);
			g.setColor(Color.cyan);
			if(selected && !end)
				g.fillRect(selXpos*sclX(100)+sclX(15) + Math.round(selXpos/1.5f), selYpos*sclY(100)+sclY(15), sclX(100), sclY(100));

			g.drawImage(new Image("game/bking.png"), boccupied[6][0]*scX+sclX(15), boccupied[6][1]*scY+sclY(15), boccupied[6][0]*scX+sclX(15)+scX, boccupied[6][1]*scY+sclY(15)+scY, 0, 0, 100, 100);
			if(boccupied[7][0] != -1)
				g.drawImage(new Image("game/bqueen.png"), boccupied[7][0]*scX+sclX(15), boccupied[7][1]*scY+sclY(15),boccupied[7][0]*scX+sclX(15)+scX, boccupied[7][1]*scY+sclY(15)+scY, 0, 0, 100, 100);
			g.drawImage(new Image("game/wking.png"), woccupied[6][0]*scX+sclX(15), woccupied[6][1]*scY+sclY(15), woccupied[6][0]*scX+sclX(15)+scX, woccupied[6][1]*scY+sclY(15)+scY, 0, 0, 100, 100);
			if(woccupied[7][0] != -1)
				g.drawImage(new Image("game/wqueen.png"), woccupied[7][0]*scX+sclX(15), woccupied[7][1]*scY+sclY(15),woccupied[7][0]*scX+sclX(15)+scX, woccupied[7][1]*scY+sclY(15)+scY, 0, 0, 100, 100);

			for (int i = 0; i < 2; i++) {
				if(boccupied[i+4][0] != -1)
					g.drawImage(new Image("game/bbishop.png"), boccupied[i+4][0]*scX+sclX(15), boccupied[i+4][1]*scY+sclY(15), boccupied[i+4][0]*scX+sclX(15)+scX, boccupied[i+4][1]*scY+sclY(15)+scY, 0, 0, 100, 100);
				if(boccupied[i+2][0] != -1)
					g.drawImage(new Image("game/bknight.png"), boccupied[i+2][0]*scX+sclX(15), boccupied[i+2][1]*scY+sclY(15), boccupied[i+2][0]*scX+sclX(15)+scX, boccupied[i+2][1]*scY+sclY(15)+scY, 0, 0, 100, 100);
				if(boccupied[i][0] != -1)
					g.drawImage(new Image("game/btower.png"), boccupied[i][0]*scX+sclX(15), boccupied[i][1]*scY+sclY(15), boccupied[i][0]*scX+sclX(15)+scX, boccupied[i][1]*scY+sclY(15)+scY, 0, 0, 100, 100);
				if(woccupied[i+4][0] != -1)
					g.drawImage(new Image("game/wbishop.png"), woccupied[i+4][0]*scX+sclX(15), woccupied[i+4][1]*scY+sclY(15), woccupied[i+4][0]*scX+sclX(15)+scX, woccupied[i+4][1]*scY+sclY(15)+scY, 0, 0, 100, 100);
				if(woccupied[i+2][0] != -1)
					g.drawImage(new Image("game/wknight.png"), woccupied[i+2][0]*scX+sclX(15), woccupied[i+2][1]*scY+sclY(15), woccupied[i+2][0]*scX+sclX(15)+scX, woccupied[i+2][1]*scY+sclY(15)+scY, 0, 0, 100, 100);
				if(woccupied[i][0] != -1)
					g.drawImage(new Image("game/wtower.png"), woccupied[i][0]*scX+sclX(15), woccupied[i][1]*scY+sclY(15), woccupied[i][0]*scX+sclX(15)+scX, woccupied[i][1]*scY+sclY(15)+scY, 0, 0, 100, 100);
			}

			for (int i = 0; i < 8; i++) {
				if(boccupied[i+8][0] != -1)
					g.drawImage(new Image("game/bpawn.png"), boccupied[i+8][0]*scX+sclX(15), boccupied[i+8][1]*scY+sclY(15), boccupied[i+8][0]*scX+sclX(15)+scX, boccupied[i+8][1]*scY+sclY(15)+scY, 0, 0, 100, 100);
				if(woccupied[i+8][0] != -1)
					g.drawImage(new Image("game/wpawn.png"), woccupied[i+8][0]*scX+sclX(15), woccupied[i+8][1]*scY+sclY(15), woccupied[i+8][0]*scX+sclX(15)+scX, woccupied[i+8][1]*scY+sclY(15)+scY, 0, 0, 100, 100);
			}

			//fontGisha.drawString(10, 800, xpos+","+ypos, Color.blue);

			textf.render(gc, g);
			
			if(!end){
				if(!surrPressed)
					g.drawImage(new Image("game/buttonsurrenderout.png"), sclX(840), sclY(835), sclX(1090), sclY(892), 0, 0, 250, 57);
				else
					g.drawImage(new Image("game/buttonsurrenderin.png"), sclX(840),sclY(835), sclX(1090), sclY(892), 0, 0, 250, 57);
				

				if(isTurn){
					g.drawImage(new Image("game/playerturn.png"), sclX(10), sclY(838), sclX(295), sclY(893), 0, 0, 285, 55);
					g.drawImage(new Image("game/player.png"), sclX(515), sclY(838), sclX(800), sclY(893), 0, 0, 285, 55);
				}

				else{
					g.drawImage(new Image("game/player.png"), sclX(10), sclY(838), sclX(295), sclY(893), 0, 0, 285, 55);
					g.drawImage(new Image("game/playerturn.png"), sclX(515), sclY(838), sclX(800), sclY(893), 0, 0, 285, 55);
				}

				fontCracked.drawString(sclX(153)-Math.round(fontCracked.getWidth(Client.user)/2f), sclY(850), Client.user);
				fontCracked.drawString(sclX(658)-Math.round(fontCracked.getWidth(opponent)/2f), sclY(850), opponent);

			}

			int l = 0;
			int i;
			for (int n = sclY(38), j = 0; j <= chat.size(); j++, n+=sclY(25)) {
				if(n >= sclY(758)){
					l = j--;
					break;
				}

				l = j;
			}

			if (chat.size() <= l)
				i = 0;
			else
				i = chat.size()-l;

			for (int j = i, y=sclY(38); j < chat.size(); j++, y+=sclY(25)) {
				if(chat.get(j).equals("YOU ARE IN CHECK"))
					fontGishaRed.drawString(sclX(854), y, chat.get(j), Color.red);
				else
					fontGisha.drawString(sclX(854), y, chat.get(j));
			}

			if(!end)
				fontStencilStd.drawString(sclX(380), sclY(845), time+"");

			if(surrender && !end){
				g.drawImage(new Image("game/surrendercheck.png"), 0, 0, resX, resY, 0, 0, 1100, 900);
				if(!yesPressed)
					g.drawImage(new Image("game/buttonyesout.png"), sclX(310), sclY(469), sclX(520), sclY(539), 0, 0, 210, 70);
				else
					g.drawImage(new Image("game/buttonyesin.png"), sclX(310), sclY(469), sclX(520), sclY(539), 0, 0, 210, 70);
				if(!noPressed)
					g.drawImage(new Image("game/buttonnoout.png"), sclX(580), sclY(470), sclX(790), sclY(540), 0, 0, 210, 70);					
				else
					g.drawImage(new Image("game/buttonnoin.png"), sclX(580), sclY(470), sclX(790), sclY(540), 0, 0, 210, 70);
			}


		}
	}

	public void setChat(String line) {
		int i = 0;
		while(fontGisha.getWidth(line) > sclX(217)){
			i = line.length();
			for (i = line.length(); i > 0; i--) {
				if(fontGisha.getWidth(line.substring(0, i)) < sclX(217))
					break;
			}

			if(line.substring(0, i).lastIndexOf(' ') > Math.round(i/2f)){
				chat.add(line.substring(0, line.substring(0, i).lastIndexOf(' ')));
				line = line.substring(line.substring(0, i).lastIndexOf(' '));
			}

			else{
				chat.add(line.substring(0, i));
				line = "-"+line.substring(i);
			}

		}
		chat.add(line);
	}

	public void /*main(String args[]) //*/startGame(int gameid, Game game, String opponent)
	{
		id = gameid;
		this.opponent = opponent;
		try
		{
			GameContainer.enableSharedContext();
			appgc = new AppGameContainer(this);
			resX = Math.round((appgc.getScreenWidth() * 1100 / 1980));
			resY = Math.round((appgc.getScreenHeight() * 900 / 1080));
			appgc.setDisplayMode(resX, resY, false);

			//appgc.setDisplayMode(1100, 900, false);
			appgc.setShowFPS(false);
			appgc.start();
		}
		catch (Exception ex)
		{
			Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
			System.out.println("exception on startGame: " + ex.getMessage());
			//System.exit(1);
		}
	}

	public void move(int[][] boccupied, int[][] woccupied) {
		this.boccupied = boccupied;
		this.woccupied = woccupied;	
		isTurn = true;
	}

	public void initPieces(){
		if(color == 0){

			boccupied[0][0] = 0; // tower
			boccupied[0][1] = 7;// tower 
			boccupied[1][0] = 7;// tower 
			boccupied[1][1] = 7;// tower 
			boccupied[2][0] = 1;//knight
			boccupied[2][1] = 7;//knight
			boccupied[3][0] = 6;//knight
			boccupied[3][1] = 7;//knight
			boccupied[4][0] = 2;//bishop
			boccupied[4][1] = 7;//bishop
			boccupied[5][0] = 5;//bishop
			boccupied[5][1] = 7;//bishop

			boccupied[6][0] = 4;//king
			boccupied[6][1] = 7;//king
			boccupied[7][0] = 3;//queen
			boccupied[7][1] = 7;//queen

			for (int i = 0; i < 8; i++) {
				boccupied[i+8][0] = i; // pawns
				boccupied[i+8][1] = 6;		// pawns
				woccupied[i+8][0] = i; // pawns
				woccupied[i+8][1] = 1;		// pawns
			}

			woccupied[0][0] = 0; // tower
			woccupied[0][1] = 0;// tower 
			woccupied[1][0] = 7;// tower 
			woccupied[1][1] = 0;// tower 
			woccupied[2][0] = 1;//knight
			woccupied[2][1] = 0;//knight
			woccupied[3][0] = 6;//knight
			woccupied[3][1] = 0;//knight
			woccupied[4][0] = 2;//bishop
			woccupied[4][1] = 0;//bishop
			woccupied[5][0] = 5;//bishop
			woccupied[5][1] = 0;//bishop



			woccupied[6][0] = 4;//king
			woccupied[6][1] = 0;//king
			woccupied[7][0] = 3;//queen
			woccupied[7][1] = 0;//queen
			isTurn = false;

		}

		else{
			woccupied[0][0] = 0; // tower
			woccupied[0][1] = 7;// tower 
			woccupied[1][0] = 7;// tower 
			woccupied[1][1] = 7;// tower 
			woccupied[2][0] = 1;//knight
			woccupied[2][1] = 7;//knight
			woccupied[3][0] = 6;//knight
			woccupied[3][1] = 7;//knight
			woccupied[4][0] = 2;//bishop
			woccupied[4][1] = 7;//bishop
			woccupied[5][0] = 5;//bishop
			woccupied[5][1] = 7;//bishop

			woccupied[6][0] = 4;//king
			woccupied[6][1] = 7;//king
			woccupied[7][0] = 3;//queen
			woccupied[7][1] = 7;//queen

			for (int i = 0; i < 8; i++) {
				woccupied[i+8][0] = i; // pawns
				woccupied[i+8][1] = 6;		// pawns
				boccupied[i+8][0] = i; // pawns
				boccupied[i+8][1] = 1;		// pawns
			}

			boccupied[0][0] = 0; // tower
			boccupied[0][1] = 0;// tower 
			boccupied[1][0] = 7;// tower 
			boccupied[1][1] = 0;// tower 
			boccupied[2][0] = 1;//knight
			boccupied[2][1] = 0;//knight
			boccupied[3][0] = 6;//knight
			boccupied[3][1] = 0;//knight
			boccupied[4][0] = 2;//bishop
			boccupied[4][1] = 0;//bishop
			boccupied[5][0] = 5;//bishop
			boccupied[5][1] = 0;//bishop

			boccupied[6][0] = 4;//king
			boccupied[6][1] = 0;//king
			boccupied[7][0] = 3;//queen
			boccupied[7][1] = 0;//queen

			isTurn = true;
		}

	}
	public void endGame(boolean isWinner, int reason) {
		end = true;
		this.reason = reason;
		this.isWinner = isWinner;
	}

	public void disable() {
		disable = true;
	}
}

package gui;

import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JButton;

import client.Client;
import requests.RequestChat;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.SystemColor;
import javax.swing.border.BevelBorder;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

public class Chat extends JPanel {
	public ArrayList<String> users;
	private ArrayList<String> text;
	private JTextField textField;
	JTextPane textPane;
	boolean isMini = false;
	public Chat(final ArrayList<String> users, final int x, int y) {
		setBackground(SystemColor.activeCaptionBorder);
		setBorder(new LineBorder(Color.ORANGE));
		
		this.users = users;
		text = new ArrayList<>();
		setLayout(null);
		setBounds(x,y,195,316);

		textField = new JTextField();

		textField.setBounds(10, 278, 143, 27);
		add(textField);
		textField.setColumns(10);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 61, 176, 206);
		add(scrollPane);

		textPane = new JTextPane();
		textPane.setForeground(SystemColor.inactiveCaptionBorder);
		textPane.setCaretColor(SystemColor.inactiveCaptionBorder);
		textPane.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		textPane.setBackground(SystemColor.desktop);
		scrollPane.setViewportView(textPane);

		final JButton btnSend = new JButton("New button");

		btnSend.setBounds(155, 274, 31, 34);
		add(btnSend);

		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSend.doClick();
			}
		});

		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!textField.getText().equals("")){
					Collections.sort(users);
					RequestChat request = new RequestChat(users, textField.getText());
					textField.setText("");
					Client.requestQueue.add(request);
				}
			}
		});
		scrollPane.setAutoscrolls(true);
		
		JPanel pnlTop = new JPanel();
		pnlTop.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		pnlTop.setBackground(SystemColor.controlDkShadow);
		pnlTop.setBounds(0, 0, 195, 50);
		add(pnlTop);
		pnlTop.setLayout(null);
		
		JButton btnMini = new JButton("New button");
		btnMini.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(isMini)
					setBounds(x, 379, 195, 316);
				else
					setBounds(x, 645, 195, 50);
				isMini ^= true;
			}
		});
		btnMini.setBounds(143, 11, 41, 30);
		pnlTop.add(btnMini);
		
		JLabel lblUser = new JLabel();
		lblUser.setBackground(SystemColor.activeCaptionBorder);
		lblUser.setHorizontalTextPosition(SwingConstants.CENTER);
		lblUser.setHorizontalAlignment(SwingConstants.CENTER);
		lblUser.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		lblUser.setFont(new Font("X-Files", Font.BOLD, 15));
		lblUser.setBounds(10, 11, 123, 30);
		for (String string : users) {
			if(!string.equals(Client.user))
				lblUser.setText(string);
				
		}
		pnlTop.add(lblUser);
		repaint();
		revalidate();
		repaint();
		setOpaque(true);
		setVisible(true);
	}

	public ArrayList<String> getText() {
		return text;
	}

	public void setText(String message) {
		textPane.setText(textPane.getText()+message+"\n");
	}
}

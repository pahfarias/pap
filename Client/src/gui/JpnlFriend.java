package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Collections;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import requests.RequestAddFriend;
import requests.RequestInvite;
import client.Client;

import java.awt.Font;

import javax.swing.SwingConstants;

import java.awt.Color;
import java.awt.Image;
import java.awt.SystemColor;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class JpnlFriend extends JPanel {
	String user;
	JButton chat;
	JButton invite;
	JButton btnAdd;
	JButton remove;
	public JpnlFriend(final String user, Boolean isSearch) {
		this.user = user;

		setLayout(null);
		this.setPreferredSize(new Dimension(240, 70));
		String user2;
		if(user.endsWith("@"))
			user2 = user.substring(0, user.length()-1);
		else
			user2 = user;
		
		
		JLabel lblUser = new JLabel(user2);
		lblUser.setForeground(new Color(0, 0, 0));
		lblUser.setHorizontalAlignment(SwingConstants.CENTER);
		lblUser.setFont(new Font("MoolBoran", Font.BOLD, 29));
		lblUser.setBounds(22, 20, 137, 39);
		add(lblUser);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(WindowMenu.class.getResource("/gui/friendbg.png")));
		label.setBounds(0, 0, 240, 70);
		

		if(isSearch){
			this.setPreferredSize(new Dimension(220, 70));
			label.setBounds(0, 0, 220, 70);
			BufferedImage img = null;
			try {
			    img = ImageIO.read(new File(WindowMenu.class.getResource("/gui/friendbg.png").toURI()));
			} catch (IOException e) {
			    e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			
			Image dimg = img.getScaledInstance(label.getWidth(), label.getHeight(),
			        Image.SCALE_SMOOTH);
			
			label.setIcon(new ImageIcon(dimg));
			btnAdd = new JButton();
			btnAdd.setBounds(165, 15, 46, 40);
			setBorder(new BevelBorder(1));
			btnAdd.setIcon(new ImageIcon(WindowMenu.class.getResource("/gui/plus.png")));
			btnAdd.setPressedIcon(new ImageIcon(WindowMenu.class.getResource("/gui/plusin.png")));
			btnAdd.setFocusPainted(false);
			btnAdd.setContentAreaFilled(false);
			btnAdd.setBorderPainted(false);
			add(btnAdd);

			btnAdd.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Client.requestQueue.add(new RequestAddFriend(user, Client.user));
					WindowMenu.btnSearch.doClick();
				}
			});
		}

		else{
			if(user.endsWith("@")){
				remove = new JButton();
				remove.setIcon(new ImageIcon(WindowMenu.class.getResource("/gui/no.png")));
				remove.setContentAreaFilled(false);
				remove.setBorderPainted(false);
				remove.setBounds(180, 15, 46, 40);
				add(remove);
				setBorder(new BevelBorder(1));

			}	

			else{
				invite = new JButton();
				invite.setBounds(180, 15, 46, 40);
				invite.setIcon(new ImageIcon(WindowMenu.class.getResource("/gui/buttoninviteout.png")));
				invite.setPressedIcon(new ImageIcon(WindowMenu.class.getResource("/gui/buttoninvitein.png")));
				invite.setContentAreaFilled(false);
				invite.setBorder(null);
				invite.revalidate();
				add(invite);

				setBorder(new BevelBorder(1));

				this.addMouseListener(new MouseAdapter(){
					public void mouseClicked(MouseEvent e){
						if(e.getClickCount()==2){
							ArrayList<String> aL = new ArrayList<String>();
							aL.add(user);
							aL.add(Client.user);
							Collections.sort(aL);
							boolean isChat = false;
							for (Chat chat : WindowMenu.chats) {
								if(aL.equals(chat.users) ){
									isChat = true;
								}
							}

							if(!isChat){
								WindowMenu.setChat(aL);
							}

						}
					}
				});

				invite.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						Client.requestQueue.add(new RequestInvite(user));
					}
				});
			}
			
			
		}
		add(label);
	}
}

package gui;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

import javax.swing.border.BevelBorder;

import requests.RequestLogin;
import client.Client;

import java.awt.SystemColor;
import javax.swing.ImageIcon;


public class JpnlLogin extends JPanel {
	/**
	 * @wbp.nonvisual location=91,-21
	 */
	private static JTextField userField;
	private static JPasswordField passwordField;

	/**
	 * Create the panel.
	 */
	public JpnlLogin() {
		setBackground(new Color(0, 0, 0));
		setLayout(null);
		setBorder(new EmptyBorder(5, 5, 5, 5));
		
		final JButton btnLogin = new JButton("");
		btnLogin.setPressedIcon(new ImageIcon(JpnlLogin.class.getResource("/gui/loginin.png")));
		btnLogin.setBorderPainted(false);
		btnLogin.setContentAreaFilled(false);
		btnLogin.setFocusPainted(false);
		btnLogin.setIcon(new ImageIcon(JpnlLogin.class.getResource("/gui/loginout.png")));
		btnLogin.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		btnLogin.setBackground(Color.LIGHT_GRAY);
		
		btnLogin.setBounds(449, 175, 204, 37);
		add(btnLogin);
		
		userField = new JTextField();
		userField.setHorizontalAlignment(SwingConstants.CENTER);
		userField.setOpaque(false);
		userField.setBorder(null);
		userField.setFont(new Font("Gisha", Font.PLAIN, 15));
		userField.setColumns(10);
		userField.setBounds(470, 72, 164, 20);
		add(userField);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Gisha", Font.PLAIN, 15));
		passwordField.setHorizontalAlignment(SwingConstants.CENTER);
		passwordField.setOpaque(false);
		passwordField.setBackground(new Color(192, 192, 192));
		passwordField.setBorder(null);
		passwordField.setColumns(10);
		passwordField.setBounds(470, 130, 164, 34);
		add(passwordField);
		
		JButton btnRegister = new JButton("");
		btnRegister.setBorderPainted(false);
		btnRegister.setContentAreaFilled(false);
		btnRegister.setPressedIcon(new ImageIcon(JpnlLogin.class.getResource("/gui/signupin.png")));
		btnRegister.setIcon(new ImageIcon(JpnlLogin.class.getResource("/gui/signupout.png")));
		btnRegister.setBorder(null);
		btnRegister.setBounds(457, 253, 190, 38);
		add(btnRegister);
		
		JLabel lblLogo = new JLabel("");
		lblLogo.setIcon(new ImageIcon(JpnlLogin.class.getResource("/gui/logo.png")));
		lblLogo.setFont(new Font("Tarzan", Font.BOLD, 17));
		lblLogo.setForeground(new Color(169, 169, 169));
		lblLogo.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogo.setBounds(25, 24, 225, 188);
		add(lblLogo);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon(JpnlLogin.class.getResource("/gui/loginbg.png")));
		lblNewLabel.setBounds(0, 0, 682, 436);
		add(lblNewLabel);
		
		setVisible(true);
		
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!userField.getText().equals("") && !passwordField.getPassword().toString().equals("")){
					if(Client.socket == null){
						Client.connect();
						btnLogin.doClick();
					}
					Client.requestQueue.add(new RequestLogin(userField.getText(),passwordField.getPassword(),true));
				}
			}
		});
		
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				WindowMain.switchPanel();
			}
		});
		
		userField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				passwordField.requestFocusInWindow();
			}
		});
		
		passwordField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnLogin.doClick();
			}
		});
		
	}
	
	public static String getUserText(){
		return userField.getText();
	}

	public static void erase() {
		passwordField.setText("");
		userField.setText("");
	}
}

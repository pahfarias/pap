package gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextArea;

import client.Client;
import requests.RequestFriendAnswer;
import requests.RequestInviteAnswer;
import responses.ResponseFriendRequest;
import responses.ResponseInvite;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.SwingConstants;
import java.awt.Color;

public class JpnlNotification extends JPanel {

	/**
	 * Create the panel.
	 * @wbp.parser.constructor
	 */
	public JpnlNotification(final ResponseFriendRequest response) {
		setLayout(null);
		this.setPreferredSize(new Dimension(250, 110));
		JButton bAccept = new JButton("");
		bAccept.setPressedIcon(new ImageIcon(JpnlNotification.class.getResource("/gui/bacceptin.png")));
		bAccept.setIcon(new ImageIcon(JpnlNotification.class.getResource("/gui/bacceptout.png")));
		bAccept.setOpaque(false);
		bAccept.setContentAreaFilled(false);
		bAccept.setBorderPainted(false);
		bAccept.setBounds(15, 68, 103, 30);
		add(bAccept);
		
		JButton bDecline = new JButton("");
		bDecline.setBorderPainted(false);
		bDecline.setContentAreaFilled(false);
		bDecline.setOpaque(false);
		bDecline.setPressedIcon(new ImageIcon(JpnlNotification.class.getResource("/gui/bdeclinein.png")));
		bDecline.setIcon(new ImageIcon(JpnlNotification.class.getResource("/gui/bdeclineout.png")));
		bDecline.setBounds(132, 68, 103, 30);
		add(bDecline);
		
		JLabel txt = new JLabel("");
		txt.setForeground(Color.BLACK);
		txt.setFont(new Font("Sansation", Font.BOLD, 16));
		txt.setHorizontalTextPosition(SwingConstants.CENTER);
		txt.setHorizontalAlignment(SwingConstants.CENTER);
		txt.setBounds(26, 15, 198, 41);
		add(txt);
		
		JLabel bg = new JLabel("");
		bg.setFont(new Font("Cracked Johnnie", Font.PLAIN, 10));
		bg.setIcon(new ImageIcon(JpnlNotification.class.getResource("/gui/notifbg.png")));
		bg.setBounds(0, 0, 250, 110);
		add(bg);
		
		
		txt.setText("<html><center>"+response.getSender().getUsername()+ "<center><p> wants to be  your friend.</html>");
		bAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Client.requestQueue.add(new RequestFriendAnswer(true, response.getIdRequest(), response.getSender(), response.getReceiver()));

				WindowMenu.notificationsList.remove(response);

				WindowMenu.setNotifications();
			}
		});

		bDecline.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Client.requestQueue.add(new RequestFriendAnswer(false, response.getIdRequest(), response.getSender(), response.getReceiver()));

				WindowMenu.notificationsList.remove(response);

				WindowMenu.setNotifications();
			}
		});

	}
	
	public JpnlNotification(final ResponseInvite responseInvite) {
		setLayout(null);
		this.setPreferredSize(new Dimension(250, 110));
		JButton bAccept = new JButton("");
		bAccept.setPressedIcon(new ImageIcon(JpnlNotification.class.getResource("/gui/bacceptin.png")));
		bAccept.setIcon(new ImageIcon(JpnlNotification.class.getResource("/gui/bacceptout.png")));
		bAccept.setOpaque(false);
		bAccept.setContentAreaFilled(false);
		bAccept.setBorderPainted(false);
		bAccept.setBounds(15, 68, 103, 30);
		add(bAccept);
		
		JButton bDecline = new JButton("");
		bDecline.setBorderPainted(false);
		bDecline.setContentAreaFilled(false);
		bDecline.setOpaque(false);
		bDecline.setPressedIcon(new ImageIcon(JpnlNotification.class.getResource("/gui/bdeclinein.png")));
		bDecline.setIcon(new ImageIcon(JpnlNotification.class.getResource("/gui/bdeclineout.png")));
		bDecline.setBounds(132, 68, 103, 30);
		add(bDecline);
		
		JLabel txt = new JLabel("");
		txt.setForeground(Color.BLACK);
		txt.setFont(new Font("Sansation", Font.BOLD, 16));
		txt.setHorizontalTextPosition(SwingConstants.CENTER);
		txt.setHorizontalAlignment(SwingConstants.CENTER);
		txt.setBounds(26, 15, 198, 37);
		add(txt);
		
		JLabel bg = new JLabel("");
		bg.setIcon(new ImageIcon(JpnlNotification.class.getResource("/gui/notifbg.png")));
		bg.setBounds(0, 0, 250, 110);
		add(bg);
		
		txt.setText("<html><center>" + responseInvite.user+ "</center><p> has challenged you.</html>");
		bAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Client.requestQueue.add(new RequestInviteAnswer(true, responseInvite.users));

				WindowMenu.notificationsList.remove(responseInvite);

				WindowMenu.setNotifications();
			}
		});

		bDecline.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Client.requestQueue.add(new RequestInviteAnswer(false, responseInvite.users));

				WindowMenu.notificationsList.remove(responseInvite);

				WindowMenu.setNotifications();
			}
		});

	}
}

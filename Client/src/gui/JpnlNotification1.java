package gui;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.BevelBorder;

import client.Client;
import requests.RequestFriendAnswer;
import requests.RequestInviteAnswer;
import responses.Response;
import responses.ResponseFriendRequest;
import responses.ResponseInvite;

public class JpnlNotification1 extends JPanel{

	public JpnlNotification1(final ResponseFriendRequest response) {
		super();
		//final ResponseFriendRequest responseIn = response;
		JLabel lbInfo;
		final JButton accept;
		JButton decline;

		setLayout(null);

		this.setPreferredSize(new Dimension(240, 100));

		lbInfo = new JLabel(response.getSender().getUsername());
		lbInfo.setBounds(10, 15, 70, 20);
		add(lbInfo);

		accept = new JButton();
		accept.setIcon(new ImageIcon("C:\\Users\\Asus\\Desktop\\yes.png"));
		accept.setBounds(90, 10, 30, 30);
		accept.setContentAreaFilled(false);
		accept.setBorderPainted(false);

		decline = new JButton();
		decline.setIcon(new ImageIcon("C:\\Users\\Asus\\Desktop\\no.png"));
		decline.setContentAreaFilled(false);
		decline.setBorderPainted(false);
		decline.setBounds(130, 10, 30, 30);

		add(accept);
		add(decline);

		setBorder(new BevelBorder(1));

		accept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Client.requestQueue.add(new RequestFriendAnswer(true, response.getIdRequest(), response.getSender(), response.getReceiver()));

				WindowMenu.notificationsList.remove(response);

				WindowMenu.setNotifications();
			}
		});

		decline.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Client.requestQueue.add(new RequestFriendAnswer(false, response.getIdRequest(), response.getSender(), response.getReceiver()));

				WindowMenu.notificationsList.remove(response);

				WindowMenu.setNotifications();
			}
		});
	}

	public JpnlNotification1(final ResponseInvite responseInvite) {
		super();
		//final ResponseFriendRequest responseIn = response;
		JLabel lbInfo;
		final JButton accept;
		JButton decline;

		setLayout(null);

		this.setPreferredSize(new Dimension(170, 50));

		lbInfo = new JLabel(responseInvite.user + " has challenged you for a duel.");
		lbInfo.setBounds(10, 15, 70, 20);
		add(lbInfo);

		accept = new JButton();
		accept.setIcon(new ImageIcon("C:\\Users\\Asus\\Desktop\\yes.png"));
		accept.setBounds(90, 10, 30, 30);
		accept.setContentAreaFilled(false);
		accept.setBorderPainted(false);

		decline = new JButton();
		decline.setIcon(new ImageIcon("C:\\Users\\Asus\\Desktop\\no.png"));
		decline.setContentAreaFilled(false);
		decline.setBorderPainted(false);
		decline.setBounds(130, 10, 30, 30);

		add(accept);
		add(decline);

		setBorder(new BevelBorder(1));

		accept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Client.requestQueue.add(new RequestInviteAnswer(true, responseInvite.users));

				WindowMenu.notificationsList.remove(responseInvite);

				WindowMenu.setNotifications();
			}
		});

		decline.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Client.requestQueue.add(new RequestInviteAnswer(false, responseInvite.users));

				WindowMenu.notificationsList.remove(responseInvite);

				WindowMenu.setNotifications();
			}
		});
	}

}
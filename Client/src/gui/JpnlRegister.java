package gui;
import javax.swing.JPanel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

import requests.RequestLogin;
import client.Client;

import java.awt.Color;
import java.awt.SystemColor;

import javax.swing.border.BevelBorder;

import java.awt.Font;

import javax.swing.border.SoftBevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.OptionPaneUI;
import javax.swing.ImageIcon;


public class JpnlRegister extends JPanel {
	private static JTextField userField;
	private static JPasswordField passwordField;

	public JpnlRegister() {
		setBackground(Color.BLACK);
		setLayout(null);

		final JButton btnRegister = new JButton("");
		btnRegister.setFocusPainted(false);
		btnRegister.setBorderPainted(false);
		btnRegister.setContentAreaFilled(false);
		btnRegister.setPressedIcon(new ImageIcon(JpnlRegister.class.getResource("/gui/signupin.png")));
		btnRegister.setIcon(new ImageIcon(JpnlRegister.class.getResource("/gui/signupout.png")));
		btnRegister.setBorder(null);
		btnRegister.setBackground(new Color(255, 140, 0));
		btnRegister.setActionCommand("");
		btnRegister.setBounds(32, 178, 195, 34);
		add(btnRegister);

		userField = new JTextField();
		userField.setFont(new Font("Gisha", Font.PLAIN, 15));
		userField.setHorizontalAlignment(SwingConstants.CENTER);
		userField.setOpaque(false);
		userField.setBorder(null);
		userField.setColumns(10);
		userField.setBounds(47, 70, 164, 23);
		add(userField);

		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Gisha", Font.PLAIN, 15));
		passwordField.setHorizontalAlignment(SwingConstants.CENTER);
		passwordField.setOpaque(false);
		passwordField.setBorder(null);
		passwordField.setColumns(10);
		passwordField.setBounds(47, 133, 161, 23);
		add(passwordField);
		setLayout(null);
		setBounds(100, 100, 680, 423);

		JButton btnBack = new JButton("");
		btnBack.setPressedIcon(new ImageIcon(JpnlRegister.class.getResource("/gui/backin.png")));
		btnBack.setIcon(new ImageIcon(JpnlRegister.class.getResource("/gui/backout.png")));
		btnBack.setFocusPainted(false);
		btnBack.setContentAreaFilled(false);
		btnBack.setBorderPainted(false);
		btnBack.setBorder(null);
		btnBack.setBackground(new Color(255, 140, 0));
		btnBack.setBounds(46, 244, 165, 38);
		add(btnBack);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(JpnlRegister.class.getResource("/gui/signupbg.png")));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Tekton Pro", Font.BOLD, 32));
		lblNewLabel.setForeground(new Color(128, 128, 128));
		lblNewLabel.setBounds(0, 0, 680, 422);
		add(lblNewLabel);
		setVisible(true);
		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(userField.getText().length() >= 4 && userField.getText().length() <= 10 && passwordField.getPassword().length >= 4 && passwordField.getPassword().length <= 10){
					boolean flag = true;
					for (char c : userField.getText().toCharArray()) {
						if(Character.getNumericValue(c)<10)
							flag = false;

					}

					if(flag){
						if(Client.socket == null)
							Client.connect();
						Client.requestQueue.add(new RequestLogin(userField.getText(),passwordField.getPassword(),false));
					}

					else {
						JOptionPane.showMessageDialog(WindowMain.jpanelMain, "The username must only contain letters.");
					}
				}

				else{
					JOptionPane.showMessageDialog(WindowMain.jpanelMain, "The username and password must have a minimum of 4 characters and a maximum of 10. ");
				}
				
			}
		});

		userField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				passwordField.requestFocusInWindow();
			}
		});

		passwordField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRegister.doClick();
			}
		});

		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				WindowMain.switchPanel();
			}
		});

	}
	
	public static void erase() {
		passwordField.setText("");
		userField.setText("");
	}
}

package gui;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import client.Client;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.ReplicateScaleFilter;
import java.awt.Toolkit;


public class WindowMain extends JFrame {
	static CardLayout cardLayout = new CardLayout(0, 0);
	public static JPanel jpanelMain = new JPanel();
	/**
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WindowMain frame = new WindowMain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/*
	 * Create the frame.
	 */
	public WindowMain() {
		setTitle("On Chess");
		setIconImage(Toolkit.getDefaultToolkit().getImage(WindowMain.class.getResource("/gui/icon.png")));
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 680, 450);
		jpanelMain.setLayout(cardLayout);
		JpnlLogin jpnlLogin = new JpnlLogin();
		JpnlRegister jpnlRegister = new JpnlRegister();
		jpanelMain.add(jpnlLogin);
		jpanelMain.add(jpnlRegister);
		getContentPane().add(jpanelMain);
		setLocationRelativeTo(null);
		setVisible(true);
	    
	}

	public static void login() {
		Client.user = JpnlLogin.getUserText();
		WindowMain window = (WindowMain) SwingUtilities.windowForComponent(jpanelMain);
		window.dispose();
		new WindowMenu();
	}

	public static void register() {
		JOptionPane.showMessageDialog(WindowMain.jpanelMain, "Register was successful! You can login to your account now.");
		switchPanel();		
	}

	public static void noLogin() {
		JOptionPane.showMessageDialog(WindowMain.jpanelMain, "Login was unsuccessful! Wrong username or password.");
		JpnlLogin.erase();
	}

	public static void noRegister() {
		JOptionPane.showMessageDialog(WindowMain.jpanelMain, "Register was unsuccessful! Try a different username.");
		JpnlRegister.erase();
	}

	public static void switchPanel() {
		cardLayout.next(jpanelMain);
	}

	public static void connecting() {
		Object[] options = { "Retry", "Exit" };
		if(JOptionPane.showOptionDialog(null, "Error connecting to server", "Error connecting to server...",JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE,null, options, options[0])==1)
			System.exit(0);	
	}
}

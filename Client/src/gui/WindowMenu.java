package gui;
import game.Game;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.Insets;

import javax.management.timer.Timer;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;

import java.awt.CardLayout;

import javax.swing.border.BevelBorder;

import java.awt.FlowLayout;

import javax.swing.JTabbedPane;

import java.awt.Rectangle;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.JTextField;

import java.awt.Color;

import javax.swing.ImageIcon;

import requests.RequestFindFriend;
import requests.RequestLogin;
import responses.Response;
import responses.ResponseFindFriend;
import responses.ResponseFriendRequest;
import responses.ResponseInvite;
import responses.ResponseStartGame;
import client.Client;
import client.WorkerThread;

import javax.swing.ScrollPaneConstants;

import java.awt.Toolkit;
import java.awt.GridLayout;
import java.awt.GridBagLayout;

import net.miginfocom.swing.MigLayout;

import java.awt.Font;

import javax.swing.SwingConstants;

import java.awt.SystemColor;

import javax.swing.JTable;


public class WindowMenu extends JFrame {

	public static JPanel contentPane;
	private CardLayout cardLayout = new CardLayout(0,0);
	private JTextField txtSearch;
	public static ArrayList<Game> gameList;
	static JButton addFriendsBtn;
	final JPanel cardPanel;
	public static final JButton notificationsBtn = new JButton();
	static GridBagConstraints gblc;
	static JPanel notifications;
	static JButton seeFriendsBtn;
	static JPanel searchResult;
	static JPanel seeFriends;
	static JPanel addFriends;
	static JButton btnSearch;

	public static ArrayList<Chat> chats;
	public ArrayList<String> friends;
	public ArrayList<String> users;
	public static ArrayList<Response> notificationsList = new ArrayList<Response>();
	private JTextField textField;
	private JLabel label;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WindowMenu frame = new WindowMenu();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public WindowMenu() {
		gameList = new ArrayList<>();
		setTitle("On Chess");
		setIconImage(Toolkit.getDefaultToolkit().getImage(WindowMenu.class.getResource("/gui/icon.png")));
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1140, 720);
		setLocationRelativeTo(null);
		
		chats = new ArrayList<>();

		contentPane = new JPanel();
		contentPane.setAlignmentY(Component.TOP_ALIGNMENT);
		contentPane.setAlignmentX(Component.LEFT_ALIGNMENT);
		contentPane.setBackground(new Color(0, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);

		setContentPane(contentPane);

		cardPanel = new JPanel();
		cardPanel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		cardPanel.setBounds(854, 55, 270, 630);
		cardPanel.setLayout(cardLayout);

		JScrollPane scrollFriends = new JScrollPane();
		scrollFriends.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollFriends.setBorder(null);
		scrollFriends.setAutoscrolls(true);
		scrollFriends.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollFriends.setBounds(5, 50, 207, 400);
		scrollFriends.getVerticalScrollBar().setUnitIncrement(5);

		seeFriends = new JPanel();
		seeFriends.setBackground(new Color(103, 103, 103));
		//seeFriends.setPreferredSize(new Dimension(180, 450));
		//seeFriends.setBounds(new Rectangle(0, 0, 180, 450));
		seeFriends.setAlignmentY(Component.TOP_ALIGNMENT);
		seeFriends.setAlignmentX(0.0f);
		scrollFriends.setViewportView(seeFriends);
		seeFriends.setBorder(null);
		seeFriends.setLayout(new MigLayout(""));


		addFriends = new JPanel();
		addFriends.setLayout(null);
		addFriends.setBackground(new Color(103, 103, 103));
		txtSearch = new JTextField();
		txtSearch.setOpaque(false);
		txtSearch.setBorder(null);
		txtSearch.setBounds(17, 16, 193, 24);
		txtSearch.setColumns(10);

		notifications = new JPanel();
		notifications.setBackground(new Color(103, 103, 103));

		addFriendsBtn = new JButton("");
		addFriendsBtn.setDisabledIcon(new ImageIcon(WindowMenu.class.getResource("/gui/baddfriendsin.png")));
		addFriendsBtn.setFocusPainted(false);
		addFriendsBtn.setContentAreaFilled(false);
		addFriendsBtn.setBorderPainted(false);
		addFriendsBtn.setBackground(Color.WHITE);
		addFriendsBtn.setIcon(new ImageIcon(WindowMenu.class.getResource("/gui/baddfriendsout.png")));
		addFriendsBtn.setBounds(1085, 10, 42, 37);

		seeFriendsBtn = new JButton("");
		seeFriendsBtn.setDisabledIcon(new ImageIcon(WindowMenu.class.getResource("/gui/bfriendsin.png")));
		seeFriendsBtn.setBackground(Color.WHITE);
		seeFriendsBtn.setIcon(new ImageIcon(WindowMenu.class.getResource("/gui/bfriendsout.png")));
		seeFriendsBtn.setBorderPainted(false);
		seeFriendsBtn.setContentAreaFilled(false);
		seeFriendsBtn.setFocusPainted(false);
		seeFriendsBtn.setEnabled(false);
		seeFriendsBtn.setBounds(1040, 10, 42, 37);

		notificationsBtn.setDisabledIcon(new ImageIcon(WindowMenu.class.getResource("/gui/bnotifin.png")));
		notificationsBtn.setIcon(new ImageIcon(WindowMenu.class.getResource("/gui/bnotifnout.png")));
		notificationsBtn.setFocusPainted(false);
		notificationsBtn.setEnabled(true);
		notificationsBtn.setContentAreaFilled(false);
		notificationsBtn.setBorderPainted(false);
		notificationsBtn.setBackground(Color.WHITE);
		notificationsBtn.setBounds(995, 10, 42, 37);

		contentPane.add(notificationsBtn);
		contentPane.add(cardPanel);
		contentPane.add(seeFriendsBtn);
		contentPane.add(addFriendsBtn);

		addFriends.add(txtSearch);

		cardPanel.add(scrollFriends, "seeFriends");
		cardPanel.add(addFriends, "addFriends");
		JScrollPane scrollSearch = new JScrollPane();
		scrollSearch.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollSearch.setBorder(null);
		scrollSearch.setAutoscrolls(true);
		scrollSearch.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollSearch.setBounds(10, 55, 250, 560);
		scrollSearch.getVerticalScrollBar().setUnitIncrement(5);

		btnSearch = new JButton("");
		btnSearch.setPressedIcon(new ImageIcon(WindowMenu.class.getResource("/gui/searchin.png")));
		btnSearch.setContentAreaFilled(false);
		btnSearch.setBackground(Color.WHITE);
		btnSearch.setBorderPainted(false);
		btnSearch.setIcon(new ImageIcon(WindowMenu.class.getResource("/gui/searchout.png")));
		btnSearch.setBounds(220, 4, 45, 44);
		addFriends.add(btnSearch);


		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(txtSearch.getText().length() >= 4)
					Client.requestQueue.add(new RequestFindFriend(txtSearch.getText()));
			}
		});
		addFriends.add(scrollSearch);

		searchResult = new JPanel();
		searchResult.setBackground(new Color(103, 103, 103));
		searchResult.setAlignmentY(Component.TOP_ALIGNMENT);
		searchResult.setAlignmentX(0.0f);
		scrollSearch.setViewportView(searchResult);
		searchResult.setBorder(null);
		searchResult.setLayout(new MigLayout(""));
		
		label = new JLabel("");
		label.setIcon(new ImageIcon(WindowMenu.class.getResource("/gui/searchbackground.png")));
		label.setBounds(0, 0, 266, 626);
		addFriends.add(label);

		cardPanel.add(notifications, "notifications");
		
		JLabel lblUser = new JLabel(Client.user);
		lblUser.setHorizontalAlignment(SwingConstants.CENTER);
		lblUser.setFont(new Font("Gisha", Font.BOLD, 16));
		lblUser.setForeground(SystemColor.desktop);
		lblUser.setBounds(859, 17, 125, 18);
		contentPane.add(lblUser);
		
		JButton button = new JButton("");
		button.setFocusPainted(false);
		button.setPressedIcon(new ImageIcon(WindowMenu.class.getResource("/gui/buttonfindgamein.png")));
		button.setBorderPainted(false);
		button.setContentAreaFilled(false);
		button.setBackground(Color.BLACK);
		button.setIcon(new ImageIcon(WindowMenu.class.getResource("/gui/buttonfindgameout.png")));
		button.setBounds(20, 17, 283, 69);
		contentPane.add(button);
		
		
		JLabel lblBackground = new JLabel("");
		lblBackground.setIcon(new ImageIcon(WindowMenu.class.getResource("/gui/menubackground.png")));
		lblBackground.setBounds(0, 0, 1134, 692);
		contentPane.add(lblBackground);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
			}
		});

		setVisible(true);

		notificationsBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cardLayout.show(cardPanel,"notifications");
				addFriendsBtn.setEnabled(true);
				seeFriendsBtn.setEnabled(true);
				notificationsBtn.setEnabled(false);
				notificationsBtn.setIcon(new ImageIcon(WindowMenu.class.getResource("/gui/bnotifnout.png")));
			}
		});

		addFriendsBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				searchResult.removeAll();
				searchResult.repaint();
				searchResult.revalidate();
				cardLayout.show(cardPanel,"addFriends");
				addFriendsBtn.setEnabled(false);
				seeFriendsBtn.setEnabled(true);
				notificationsBtn.setEnabled(true);
			}
		});

		seeFriendsBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cardLayout.show(cardPanel,"seeFriends");
				seeFriendsBtn.setEnabled(false);
				addFriendsBtn.setEnabled(true);
				notificationsBtn.setEnabled(true);
				
			}
		});

		txtSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSearch.doClick();
			}
		});
	}

	public static void setFriends(ResponseFindFriend response) {
		//if(!addFriendsBtn.isEnabled() ){
		if(response.isSearch){
			searchResult.removeAll();
			searchResult.repaint();
			int row = 0;

			while(!response.getUsers().isEmpty()){
				searchResult.add(new JpnlFriend(response.getUsers().remove(0),true),"cell 0 "+ (++row) +",growx");
			}

			searchResult.revalidate();
		}

		//else if(!seeFriendsBtn.isEnabled() || !notificationsBtn.isEnabled()){
		else{
			seeFriends.removeAll();
			seeFriends.repaint();
			int row = 0;

			if(!response.getUsers().isEmpty()){
				seeFriends.add(new JLabel(new ImageIcon(WindowMenu.class.getResource("/gui/lblOnline.png"))),"cell 0 "+ (++row) +",growx");
				
				while(!response.getUsers().isEmpty())
					seeFriends.add(new JpnlFriend(response.getUsers().remove(0),false),"cell 0 "+ (++row) +",growx");
			}

			if(!response.getUsersOff().isEmpty()){
				seeFriends.add(new JLabel(new ImageIcon(WindowMenu.class.getResource("/gui/lblOffline.png"))),"cell 0 "+ (++row) +",growx");
				while(!response.getUsersOff().isEmpty()){
					seeFriends.add(new JpnlFriend(response.getUsersOff().remove(0)+"@",false),"cell 0 "+ (++row) +",growx");
				}
			}
			seeFriends.revalidate();
		}

	}

	public static void setNotifications() {
		notifications.removeAll();
		notifications.repaint();
		for (int i = 0; i < notificationsList.size(); i++) {
			if (notificationsList.get(i) instanceof ResponseFriendRequest){
				notifications.add(new JpnlNotification((ResponseFriendRequest) notificationsList.get(i)));
				notificationsBtn.setIcon(new ImageIcon(WindowMenu.class.getResource("/gui/bnotifout.png")));
			}
			if (notificationsList.get(i) instanceof ResponseInvite){
				notifications.add(new JpnlNotification((ResponseInvite) notificationsList.get(i)));
				notificationsBtn.setIcon(new ImageIcon(WindowMenu.class.getResource("/gui/bnotifout.png")));
			}
		}

		notifications.revalidate();

	}

	public static void setChat(ArrayList<String> aL) {
		contentPane.repaint();
		Chat chat = new Chat(aL,chats.size()*200,contentPane.getHeight()-316);
		chat.repaint();
		chat.revalidate();
		contentPane.add(chat);
		chats.add(chat);
		contentPane.revalidate();
	}

	public static void setChat(ArrayList<String> aL, String message) {
		Chat chat = new Chat(aL,chats.size()*200,contentPane.getHeight()-316);
		chat.setText(message);
		contentPane.add(chat);
		chats.add(chat);
	}

	public static void startGame(int id, String opponent) {
		WindowMenu window = (WindowMenu) SwingUtilities.windowForComponent(contentPane);
		window.setVisible(false);
		gameList.clear();
		gameList.add(new Game("OnChess"));
		gameList.get(gameList.size()-1).startGame(id,gameList.get(gameList.size()-1), opponent);
	}

	public static void endGame() {
		gameList.get(gameList.size()-1).disable();
		WindowMenu window = (WindowMenu) SwingUtilities.windowForComponent(contentPane);
		window.setVisible(true);
	}
}





package requests;

import java.util.ArrayList;

public class RequestChat extends Request{
	private static final long serialVersionUID = 1L;
	ArrayList<String> users;
	String message;
	
	public RequestChat(ArrayList<String> users,String message){
		this.users=users;
		this.message = message;
	}
}

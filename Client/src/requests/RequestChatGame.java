package requests;

public class RequestChatGame extends Request{
	private static final long serialVersionUID = 1L;
	public int id;
	public String username,chat;
	
	public RequestChatGame(int id, String user, String chat){
		this.id = id;
		username = user;
		this.chat = chat;	
	}
}

package requests;

public class RequestColor extends Request{
	private static final long serialVersionUID = 1L;
	public int id, color;
	public String username;
	
	
	public RequestColor( int color, int gameid, String user){
		this.color = color;
		id = gameid;
		username = user;
	}
}

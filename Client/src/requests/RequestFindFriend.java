package requests;
import java.util.ArrayList;


public class RequestFindFriend extends Request {
	private static final long serialVersionUID = 1L;
	private String search;
	
	public RequestFindFriend(String search){
		this.search=search;
	}

	public String getSearch() {
		return search;
	}

}
package requests;

import user.User;

public class RequestFriendAnswer extends Request{
	private boolean answer;
	private int idRequest; 
	private User sender, receiver;
	
	public RequestFriendAnswer(boolean answer, int idRequest, User sender, User receiver) {
		this.answer = answer;
		this.idRequest = idRequest;
		this.setSender(sender);
		this.setReceiver(receiver);
	}
	
	public boolean getAnswer() {
		return answer;
	}

	public int getIdRequest() {
		return idRequest;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}





}

package requests;

import java.util.ArrayList;

public class RequestInviteAnswer extends Request {
	private static final long serialVersionUID = 1L;
	ArrayList<String> users;
	boolean answer;
	public RequestInviteAnswer(boolean answer, ArrayList<String> users){
		this.users = users;
		this.answer = answer;
	}
}

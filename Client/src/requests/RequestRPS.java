package requests;

public class RequestRPS extends Request{
	private static final long serialVersionUID = 1L;
	public int rps;
	public int id;
	public String username;
	public RequestRPS(int rps, int gameid, String user){
		this.rps = rps;
		id = gameid;
		username = user;
	}
}

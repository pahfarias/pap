package requests;

public class RequestSurrender extends Request{
	private static final long serialVersionUID = 1L;
	public String user;
	public int id;
	public RequestSurrender(int id, String user){
		this.id = id;
		this.user = user;	
	}

}

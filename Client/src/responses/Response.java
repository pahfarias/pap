package responses;
import java.io.Serializable;

public abstract class Response implements Serializable{

	private static final long serialVersionUID = 1L;

	protected Response() {
	}
}
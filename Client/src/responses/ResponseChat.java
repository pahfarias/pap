package responses;

import java.util.ArrayList;

public class ResponseChat extends Response{
	public String message;
	public ArrayList<String> users;
	public ResponseChat(String message, ArrayList<String> users){
		this.message = message;
		this.users = users;
		
	}
	
}

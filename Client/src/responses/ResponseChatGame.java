package responses;

public class ResponseChatGame extends Response{
	private static final long serialVersionUID = 1L;
	public String chat;
	
	public ResponseChatGame(String chat){
		this.chat = chat;
	}

}

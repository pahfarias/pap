package responses;

public class ResponseColor extends Response{
	private static final long serialVersionUID = 1L;
	public int color;
	
	public ResponseColor(int color){
		this.color = color;
	}
}

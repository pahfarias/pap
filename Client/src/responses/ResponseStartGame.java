package responses;

import java.util.ArrayList;

public class ResponseStartGame extends Response{
	private static final long serialVersionUID = 1L;
	public ArrayList<String> users;
	public int id;
	public String opponent;
	
	public ResponseStartGame(ArrayList<String> users, int id, String opp){
		opponent = opp;
		this.users = users;
		this.id = id;
	}
}

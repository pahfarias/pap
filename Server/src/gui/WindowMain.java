package gui;
import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.ScrollPane;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.ImageIcon;
import javax.swing.JTextPane;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.awt.Font;
import java.awt.Color;

import javax.swing.JEditorPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;

import java.awt.Rectangle;
import java.awt.Insets;
import java.io.IOException;

import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import server.ClientThread;
import server.Server;
import user.User;

import java.awt.SystemColor;


public class WindowMain extends JFrame {

	private JPanel contentPane;
	private static JTextPane txtpnServer; 
	private static StyledDocument styleDoc;
	private static SimpleAttributeSet style;
	private JScrollPane scrollPane;
	
	public static Object lock = new Object();
	private JButton btnOnline;
	private JButton btnShutdown;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WindowMain frame = new WindowMain();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public WindowMain() {
		setResizable(false);

		setTitle("OnChess Server");
		setIconImage(Toolkit.getDefaultToolkit().getImage(WindowMain.class.getResource("/gui/icon.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 567, 716);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.activeCaption);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setAutoscrolls(true);
		scrollPane.setBounds(5, 11, 414, 668);
		contentPane.add(scrollPane);

		txtpnServer = new JTextPane();
		txtpnServer.setFont(new Font("Tahoma", Font.PLAIN, 14));

		scrollPane.setViewportView(txtpnServer);

		txtpnServer.setMargin(new Insets(10, 10, 10, 10));
		txtpnServer.setBorder(new LineBorder(new Color(0, 0, 0)));
		txtpnServer.setFocusable(false);
		txtpnServer.setDisabledTextColor(Color.BLACK);
		
		btnOnline = new JButton("List online users");
		btnOnline.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		btnOnline.setBorderPainted(false);
		btnOnline.setFocusTraversalKeysEnabled(false);
		btnOnline.setFocusPainted(false);
		btnOnline.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnOnline.setBounds(429, 11, 124, 36);
		contentPane.add(btnOnline);
		
		btnShutdown = new JButton("Shutdown");
		btnShutdown.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnShutdown.setFocusTraversalKeysEnabled(false);
		btnShutdown.setFocusPainted(false);
		btnShutdown.setBorderPainted(false);
		btnShutdown.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		btnShutdown.setBounds(429, 643, 124, 36);
		contentPane.add(btnShutdown);
		styleDoc = txtpnServer.getStyledDocument();
		style = new SimpleAttributeSet();

		try {
			styleDoc.insertString(styleDoc.getLength(), "\n",style);
		} catch (BadLocationException e) {}

		setVisible(true);
		
		btnOnline.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				processCommand("online");
				
			}
		});
		
		btnShutdown.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				processCommand("shutdown");
			}
		});

	}

	public void processCommand(String command) {
		if(command.toLowerCase().equals("shutdown")){
			System.exit(0);
		}

		else if(command.toLowerCase().trim().equals("online")){
			String users = "";
			if(Server.onlineUsers.isEmpty())
				users = "There are no users online.";

			else{
				users = "Users online: ";
				for (User user : Server.onlineUsers) {
					users += user.getUsername() + ", ";
				}
				users = users.substring(0, (users.length()-2));
				users+= ".";
			}
			writeCommand(users);
		}

		else{
			writeError("Error: Command not recognized \""+command+"\"");

		}

	}

	public static void writeInfo(String info){
		StyleConstants.setForeground(style, new Color(1,127,255));
		try {
			styleDoc.insertString(styleDoc.getLength(), ">>  "+info+"\n\n",style);
			txtpnServer.setCaretPosition(styleDoc.getLength());
		} catch (Exception e) {
			System.out.println(txtpnServer.getText().length() + "-" + styleDoc.getLength());
		}

	}

	public static void writeError(String error){
		StyleConstants.setForeground(style, new Color(238,59,59));
		try {
			styleDoc.insertString(styleDoc.getLength(), ">>  "+error+"\n\n",style);
			txtpnServer.setCaretPosition(styleDoc.getLength());
		} catch (Exception e) {
			System.out.println(txtpnServer.getText().length() + "-" + styleDoc.getLength());
		}


	}

	public static void writeCommand(String command){
		StyleConstants.setForeground(style, new Color(69,139,116));
		try {
			styleDoc.insertString(styleDoc.getLength(),">>  " + command+"\n\n",style);
			txtpnServer.setCaretPosition(styleDoc.getLength());
		} catch (Exception e) {
			System.out.println(txtpnServer.getText().length() + "-" + styleDoc.getLength());
		}


	}
}

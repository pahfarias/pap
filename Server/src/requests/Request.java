package requests;
import java.io.*;

import user.User;

public abstract class Request implements Serializable{
	private static final long serialVersionUID = -1362997022565309362L;
	private ObjectOutputStream out;
	private User user;
	public Request() {

	}
	
	public void setOut(ObjectOutputStream out) {
		this.out = out;
	}
	
	public ObjectOutputStream getOut() {
		return out;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
}
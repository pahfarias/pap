package requests;

public class RequestMove extends Request {
	private static final long serialVersionUID = 1L;
	public int[][] boccupied = new int[16][2], woccupied = new int[16][2];
	public int id;
	public String user;

	public RequestMove(int id, String user, int[][] boccupied, int[][] woccupied) {
		this.user = user;
		this.id = id;
		this.boccupied = boccupied;
		this.woccupied = woccupied;
	}
	
}

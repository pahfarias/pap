package responses;
import java.io.*;

public class ResponseBoolean extends Response{
	private boolean isSuccessful;
	private boolean isRegistered;
	private boolean isNotOn;
	public ResponseBoolean(boolean isSuccessful, boolean isRegistered){
		this.setSuccessful(isSuccessful);
		this.setRegistered(isRegistered);
	}

	public ResponseBoolean(boolean isSuccessful, boolean isRegistered, boolean isNotOn) {
		this.setSuccessful(isSuccessful);
		this.setRegistered(isRegistered);
		this.setNotOn(isNotOn);
	}

	public boolean isRegistered() {
		return isRegistered;
	}

	public void setRegistered(boolean isRegistered) {
		this.isRegistered = isRegistered;
	}

	public boolean isSuccessful() {
		return isSuccessful;
	}

	public void setSuccessful(boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}

	public boolean isNotOn() {
		return isNotOn;
	}

	public void setNotOn(boolean isNotOn) {
		this.isNotOn = isNotOn;
	}
	
}
package responses;

public class ResponseEndGame extends Response{
	private static final long serialVersionUID = 1L;
	int reason;
	boolean isWinner;
	
	public ResponseEndGame(boolean isWinner, int reason) {
		this.isWinner = isWinner;
		this.reason = reason;
	}
}

package responses;
import java.util.ArrayList;


public class ResponseFindFriend extends Response{
	private static final long serialVersionUID = 1L;
	private ArrayList<String> users,usersOff;
	public boolean isSearch;
	
	public ResponseFindFriend(ArrayList<String> users, ArrayList<String> usersOff, boolean isSearch) {
		this.isSearch = isSearch;
		setUsers(users);
		setUsersOff(usersOff);
	}


	public ArrayList<String> getUsers() {
		return users;
	}


	public void setUsers(ArrayList<String> users) {
		this.users = users;
	}


	public ArrayList<String> getUsersOff() {
		return usersOff;
	}


	public void setUsersOff(ArrayList<String> usersOff) {
		this.usersOff = usersOff;
	}

}

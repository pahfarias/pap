package responses;

import java.io.Serializable;

import user.User;

public class ResponseFriendRequest extends Response{

	private static final long serialVersionUID = 1L;
	private User sender;
	private User receiver;
	private int idRequest;

	
	public ResponseFriendRequest() {
	}
	
	public ResponseFriendRequest(User sender, User receiver, int idRequest) {
		this.setSender(sender);
		this.setReceiver(receiver);
		this.setIdRequest(idRequest);
	}

	public int getIdRequest() {
		return idRequest;
	}

	public void setIdRequest(int idRequest) {
		this.idRequest = idRequest;
	}

	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

}

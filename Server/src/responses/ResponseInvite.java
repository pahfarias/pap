package responses;

import java.util.ArrayList;

public class ResponseInvite extends Response{
	public String user;
	public ArrayList<String> users = new ArrayList<String>();
	public ResponseInvite(String user, String user2){
		this.user = user;
		users.add(user);
		users.add(user2);
	}
}

package responses;

public class ResponseMove extends Response {
	private static final long serialVersionUID = 1L;
	public int[][] boccupied = new int[16][2], woccupied = new int[16][2];
	
	public ResponseMove(int[][] boccupied, int[][] woccupied) {
		this.boccupied = boccupied;
		this.woccupied = woccupied;
	}

}

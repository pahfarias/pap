package server;
import gui.WindowMain;

import java.io.*;
import java.util.*;
import java.net.Socket;
import java.util.Queue;

import requests.Request;
import requests.RequestFindFriend;
import responses.ResponseFindFriend;
import user.User;

public class ClientThread extends Thread{
	public ObjectInputStream in;
	public ObjectOutputStream out;
	private Socket socket;
	private User user;
	private int gameId;
	public ClientThread(Socket client) {
		socket = client;
		user = new User();
		setGameId(-1);
		try{
			in = new ObjectInputStream(socket.getInputStream());
			out = new ObjectOutputStream(socket.getOutputStream());
			this.start();
		}catch(IOException e){System.out.println("Connection:" + e.getMessage());}
	}

	public void run() {
		try{
			while(true){
				Request request = (Request) in.readObject();
				request.setOut(out);
				if(user.getId()!=0)
					request.setUser(user);
				WorkerThread.requests.add(request);
			}
		}catch(EOFException e){System.out.println("EOF:" + e);
		}catch(IOException e){
			if(e.toString().trim().equals("java.net.SocketException: Connection reset"))
				if(user.getUsername()!=""){
					WindowMain.writeInfo("User: '"+ user.getUsername()+"' at '"+socket.getInetAddress()+"' has disconnected.");
					
					int i = 0;
					
					for (User userOn : Server.onlineUsers) {
						if(userOn == user)
							break;
						i++;
					}
					
					for (Game game : WorkerThread.gameList) {
						if(game.id == getGameId())
							try {
								game.disconnect(getUser().getUsername());
							} catch (IOException e1) {}
					}
					
					Server.clientThreads.remove(this);
					Server.onlineUsers.remove(i);	
					
					for (User user : Server.onlineUsers)
						for (ClientThread ct : Server.clientThreads)
							if(user.getUsername().equals(ct.getUser().getUsername()))
								try {
									ct.out.writeObject(new ResponseFindFriend(Server.dbConn.findFriends(new RequestFindFriend(null),0, user.getId()),Server.dbConn.findFriends(new RequestFindFriend(null),1,user.getId()),false));
								} catch (IOException e1) {}
				}
				else
					WindowMain.writeInfo("Client at '"+socket.getInetAddress()+"' has disconnected.");
			else
				WindowMain.writeError("IO Error: " + e);
		}catch (ClassNotFoundException e){
		}
	}

	public void clientThread(Socket client) {
		socket = client;
		try{
			in = new ObjectInputStream(socket.getInputStream());
			out = new ObjectOutputStream(socket.getOutputStream());
			this.start();
		}catch(IOException e){System.out.println("Connection:" + e.getMessage());}
	}

	public void setOnline(){
		
	}
	
	public User getUser() {
		return user;
	}

	public ObjectOutputStream getOut() {
		return out;
	}

	public void setOut(ObjectOutputStream out) {
		this.out = out;
	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}
}
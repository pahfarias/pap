package server;
import gui.WindowMain;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;

import requests.RequestFindFriend;
import requests.RequestFriendAnswer;
import responses.ResponseFindFriend;
import responses.ResponseFriendRequest;
import user.User;

public class DatabaseManager {
	static Connection db;

	public DatabaseManager(){
		try {
			Class.forName("org.postgresql.Driver");
			db = DriverManager.getConnection("jdbc:postgresql://localhost/test", "postgres", "admin");
			db.prepareStatement("CREATE TABLE IF NOT EXISTS Users(Id serial, Username text not null, Password text not null, primary key (Id))").execute();
			db.prepareStatement("CREATE TABLE IF NOT EXISTS Friends(Id serial, UserId int not null, Friend int not null, primary key (Id))").execute();
			db.prepareStatement("CREATE TABLE IF NOT EXISTS FriendRequests(Id serial, UserId int not null, Friend int not null, primary key (Id))").execute();
			WindowMain.writeInfo("Connection to database successful.");
		} catch (ClassNotFoundException e) {WindowMain.writeError("Problem connecting to database: " + e );
		} catch (SQLException e) {WindowMain.writeError("Database not found: " + e);}
	}

	public int checkUser(String user){
		try {
			ResultSet rs = db.prepareStatement("SELECT id,username from users").executeQuery();
			while(rs.next()){
				if(user.equalsIgnoreCase(rs.getString(2)))
					return rs.getInt(1);
			}
		} catch (SQLException e) {WindowMain.writeError("Problem checking user." + e); return 0;}
		return 0;
	}

	public boolean checkPassword(String password, String user){
		try {
			ResultSet rs = db.prepareStatement("SELECT password from users where username = '"+user+"'").executeQuery();
			while(rs.next()){
				if(password.equals(rs.getString(1)))
					return true;
			}
		} catch (SQLException e) {WindowMain.writeError("Problem checking user." + e); return false;}
		return false;
	}

	public boolean addUser(String user, String password){
		try {
			db.prepareStatement("INSERT INTO Users(username,password) Values('"+user+"','"+password+"')").executeUpdate();
		} catch (SQLException e) {WindowMain.writeError("Problem adding user to database." + e); return false;}

		return true;
	}

	public ArrayList<String> findFriends(RequestFindFriend request, int i, int id) {
		ArrayList<String> users = new ArrayList<String>(); 
		try {
			boolean flag = true;
			if(request.getSearch() != null){
				ResultSet rs = db.prepareStatement("SELECT id,username from users").executeQuery();
				while(rs.next()){
					flag = true;
					if(!request.getUser().getUsername().equals(rs.getString(2)) && rs.getString(2).startsWith(request.getSearch())){
						ResultSet rs2 = db.prepareStatement("SELECT friend from friends where userid = '"+ request.getUser().getId() +"'").executeQuery();

						while(rs2.next())
							if(rs.getInt(1) == rs2.getInt(1))
								flag = false;

						rs2 = db.prepareStatement("SELECT friend from friendrequests where userid = '"+ request.getUser().getId() +"'").executeQuery();

						while(rs2.next())
							if(rs.getInt(1) == rs2.getInt(1))
								flag = false;

						rs2 = db.prepareStatement("SELECT userid from friendrequests where friend = '"+ request.getUser().getId() +"'").executeQuery();

						while(rs2.next())
							if(rs.getInt(1) == rs2.getInt(1))
								flag = false;


						if(flag){
							users.add(rs.getString(2));

						}
					}
				}
			}

			else{
				ResultSet rs,rs2;
				if(id>=0)
					rs = db.prepareStatement("SELECT friend from friends where userid = '"+ id +"'").executeQuery();
				else
					rs = db.prepareStatement("SELECT friend from friends where userid = '"+ request.getUser().getId() +"'").executeQuery();

				while(rs.next()){
					rs2 = db.prepareStatement("Select username from users where id = '"+ rs.getInt(1) +"'").executeQuery();
					rs2.next();
					if(i==0)
						for (User userOn : Server.onlineUsers) {
							if(userOn.getUsername().equals(rs2.getString(1))){
								users.add(rs2.getString(1));
							}
						}

					else{
						flag = true;
						for (User userOn : Server.onlineUsers) {
							if(userOn.getUsername().equals(rs2.getString(1)))
								flag = false;
						}
						if(flag){
							users.add(rs2.getString(1));
						}
					}
				}


			}
		} catch (SQLException e) {}

		return users;
	}


	public void addFriendRequest(User user, String friend) {

		try {
			ResultSet rs = db.prepareStatement("Select id from users where username = '"+ friend +"'").executeQuery();
			rs.next();
			int idFriend = rs.getInt(1);
			db.prepareStatement("INSERT INTO friendrequests(userid,friend) Values('"+user.getId()+"','"+idFriend+"')").executeUpdate();
		} catch (SQLException e) {
		}
		WindowMain.writeInfo("Friend request from "+user.getUsername()+ " to "+friend+" was inserted.");
	}

	//Processa as respostas aos pedidos de amizade
	public void friendRequestAnswer(RequestFriendAnswer request) {
		WindowMain.writeInfo("Request Friend Answer");
		if(request.getAnswer()){

			//Se o utilizador aceitar o pedido, s�o introduzidos na lista de amigos

			try {
				db.prepareStatement("INSERT INTO friends(userid,friend) Values('"+request.getSender().getId()+"','"+request.getReceiver().getId()+"')").executeUpdate();
				db.prepareStatement("INSERT INTO friends(userid,friend) Values('"+request.getReceiver().getId()+"','"+request.getSender().getId()+"')").executeUpdate();
			} catch (SQLException e) {
			}
		}
		// O pedido � apagado da lista de pedidos

		try {
			db.prepareStatement("DELETE from friendrequests where id = '"+request.getIdRequest()+"'").executeUpdate();
		} catch (SQLException e) {}

		

		sendFriendRequests();

	}

	public void sendFriendRequests() {
		synchronized (db) {
			for (User user : Server.onlineUsers) {
				try {
					ResultSet rs = db.prepareStatement("Select id, userid from friendrequests where friend = '"+user.getId()+"'").executeQuery();
					while(rs.next()){
						ResultSet rs1 = db.prepareStatement("Select username from users where id = '"+rs.getInt(2)+"'").executeQuery();
						rs1.next();

						for (ClientThread clientThread : Server.clientThreads) {
							if(user.getId() == clientThread.getUser().getId())
								clientThread.getOut().writeObject(new ResponseFriendRequest(new User(rs1.getString(1),rs.getInt(2)), user, rs.getInt(1)));
						}

						WindowMain.writeInfo("Request sent to "+user.getUsername()+" from "+rs1.getString(1)+".");

					}
				} catch (SQLException | IOException e) { WindowMain.writeError(e+"");}
			}
		}





	}

}

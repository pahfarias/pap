package server;

import java.util.ArrayList;
import java.util.Collections;
import java.io.*;

import responses.ResponseChatGame;
import responses.ResponseColor;
import responses.ResponseEndGame;
import responses.ResponseMove;
import responses.ResponseRPS;
import responses.ResponseStartGame;

public class Game {
	public int id, p1RPS=0, p2RPS=0, p1Col = -1, p2Col;
	public ArrayList<String> users;
	public ObjectOutputStream p1Out = null ,p2Out;
	public String player1, player2;
	public boolean end = false, isRPS = true;
	public Game(ArrayList<String> users, int gameid) {
		id = gameid;
		System.out.println(id);
		this.users = users;
		Collections.sort(users);
		for (String user : users) {
			for (ClientThread ct : Server.clientThreads) {
				if(ct.getUser().getUsername().equals(user))

					if(p1Out == null){
						player1 = user;
						p1Out = ct.getOut();
						ct.setGameId(id);
					}
					else{
						player2 = user;
						ct.setGameId(id);
						p2Out = ct.getOut();
					}

			}
		}
		try {
			p1Out.writeObject(new ResponseStartGame(users, id, player2));
			p2Out.writeObject(new ResponseStartGame(users, id, player1));
		} catch (IOException e) {
		}
	}

	public void rps(int rps, String user) {
		if(user.equals(player1))
			p1RPS = rps;
		else
			p2RPS = rps;

		if(p1RPS != 0 && p2RPS != 0){
			try {
				p1Out.writeObject(new ResponseRPS(p2RPS));
				p2Out.writeObject(new ResponseRPS(p1RPS));
			} catch (IOException e) {
				System.out.println("?");
			}

			if(p1RPS == p2RPS){
				p1RPS = 0;
				p2RPS = 0;
			}
			
			else
				isRPS = false;

		}

	}
	public void color(int color, String user) {
		if(user.equals(player1)){
			p1Col = color;
			if(color == 0)
				p2Col = 1;
			else
				p2Col = 0;
		}

		else{
			p2Col = color;
			if(color == 0)
				p1Col = 1;
			else
				p1Col = 0;
		}


		try {
			p1Out.writeObject(new ResponseColor(p1Col));
			p2Out.writeObject(new ResponseColor(p2Col));
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	public void chat(String chat, String user) {
		try {
			p1Out.writeObject(new ResponseChatGame(user + ": " + chat));
			p2Out.writeObject(new ResponseChatGame(user + ": " + chat));
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	public void move(int[][] boccupied, int[][] woccupied, String user) {
		for (int i = 0; i < woccupied.length; i++) {
			for (int j = 0; j < woccupied[i].length; j++) {
				switch (woccupied[i][j]) {
				case 0: woccupied[i][j] = 7; break;
				case 1: woccupied[i][j] = 6; break;
				case 2: woccupied[i][j] = 5; break;
				case 3: woccupied[i][j] = 4; break;
				case 4: woccupied[i][j] = 3; break;
				case 5: woccupied[i][j] = 2; break;
				case 6: woccupied[i][j] = 1; break;
				case 7: woccupied[i][j] = 0; break;
				}

				switch (boccupied[i][j]) {
				case 0: boccupied[i][j] = 7; break;
				case 1: boccupied[i][j] = 6; break;
				case 2: boccupied[i][j] = 5; break;
				case 3: boccupied[i][j] = 4; break;
				case 4: boccupied[i][j] = 3; break;
				case 5: boccupied[i][j] = 2; break;
				case 6: boccupied[i][j] = 1; break;
				case 7: boccupied[i][j] = 0; break;
				}
			}
		}


		if(user.equals(player1)){
			try {
				p2Out.writeObject(new ResponseMove(boccupied, woccupied));
			} catch (IOException e) {}
		}

		else{
			try {
				p1Out.writeObject(new ResponseMove(boccupied, woccupied));
			} catch (IOException e) {}
		}

	}

	public void disconnect(String user) throws IOException {
		if(!end){
			if(user.equals(player1)){
				p2Out.writeObject(new ResponseEndGame(true, 1));
				if(!isRPS);
				//TODO put victory in db
			}
			else
				p1Out.writeObject(new ResponseEndGame(true, 1));
		}
	}

	public void surrender(String user) throws IOException {
		end = true;
		if(user.equals(player1)){
			p2Out.writeObject(new ResponseEndGame(true, 2));
			p1Out.writeObject(new ResponseEndGame(false, 2));
			//TODO put victory in db
		}
		else{
			p1Out.writeObject(new ResponseEndGame(true, 2));
			p2Out.writeObject(new ResponseEndGame(false, 2));
		}
	}

	public void checkmate(String user) throws IOException {
		end = true;
		
		if(user.equals(player1)){
			p2Out.writeObject(new ResponseEndGame(true, 0));
			p1Out.writeObject(new ResponseEndGame(false, 0));
			//TODO put victory in db
		}
		else{
			p1Out.writeObject(new ResponseEndGame(true, 0));
			p2Out.writeObject(new ResponseEndGame(false, 0));
		}
	}
}

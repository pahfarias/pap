package server;
import gui.WindowMain;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import user.User;

public class Server extends Thread{
	public static List<ClientThread> clientThreads = Collections.synchronizedList(new ArrayList<ClientThread>()); ;
	private ArrayList<WorkerThread> workers;
	private ServerSocket listenSocket;
	public static DatabaseManager dbConn;
	public static List<User> onlineUsers = Collections.synchronizedList(new ArrayList<User>()); ;
	public static void main(String args[]) {
		new WindowMain();
		dbConn = new DatabaseManager();
		Server server = new Server();
		server.run();
	}

	protected Server() {
		try{
			int serverPort = 6000;
			workers = new ArrayList<>();
			for (int i = 0; i < 4; i++) {
				workers.add(new WorkerThread());
			}
			listenSocket = new ServerSocket(serverPort);
			WindowMain.writeInfo("Listening on Port "+serverPort);
		}catch(IOException e)
		{WindowMain.writeError("Error: " + e.getMessage());}
	}

	public void run() {
		try{
			while(true) {
				Socket clientSocket = listenSocket.accept();
				WindowMain.writeInfo("New client -> "+clientSocket);
				//if(clientThreads.peek()==null)
				clientThreads.add(new ClientThread(clientSocket));
				//else
					//clientThreads.poll().clientThread(clientSocket);
				
			}
		//}catch(IOException e)
		}catch(Exception e){
		}
	}
}
package server;
import gui.WindowMain;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import requests.Request;
import requests.RequestAddFriend;
import requests.RequestChat;
import requests.RequestChatGame;
import requests.RequestCheckMate;
import requests.RequestColor;
import requests.RequestFindFriend;
import requests.RequestFriendAnswer;
import requests.RequestInvite;
import requests.RequestInviteAnswer;
import requests.RequestLogin;
import requests.RequestMove;
import requests.RequestRPS;
import requests.RequestSurrender;
import responses.ResponseBoolean;
import responses.ResponseChat;
import responses.ResponseFindFriend;
import responses.ResponseFriendRequest;
import responses.ResponseInvite;
import user.User;

public class WorkerThread extends Thread{
	public static int gameid = 0;
	public static ArrayBlockingQueue<Request> requests = new ArrayBlockingQueue<>(100);
	public static ArrayList<Game> gameList = new ArrayList<Game>(50);
	public WorkerThread() {
		WindowMain.writeInfo("Creating Worker Thread...");
		this.start();
	}

	public void run() {
		while(true){
			try {
				processrequest(requests.take());
			} catch (InterruptedException e) {
			} catch (IOException e) {

			}
		}
	}

	private void processrequest(Request request) throws IOException {
		if (request instanceof RequestLogin) 
			requestLogin((RequestLogin) request);
		else if (request instanceof RequestFindFriend) 
			requestFindFriend((RequestFindFriend) request);
		else if(request instanceof RequestAddFriend)
			requestAddFriend((RequestAddFriend) request);
		else if(request instanceof RequestFriendAnswer)
			requestFriendAnswer((RequestFriendAnswer) request);
		else if(request instanceof RequestChat)
			requestChat((RequestChat) request);
		else if(request instanceof RequestInvite)
			requestInvite((RequestInvite) request);
		else if(request instanceof RequestInviteAnswer)
			requestInviteAnswer((RequestInviteAnswer) request);
		else if(request instanceof RequestRPS)
			requestRPS((RequestRPS) request);
		else if(request instanceof RequestColor)
			requestColor((RequestColor) request);
		else if(request instanceof RequestChatGame)
			requestChatGame((RequestChatGame) request);
		else if(request instanceof RequestMove)
			requestMove((RequestMove) request);
		else if(request instanceof RequestCheckMate)
			requestCheckMate((RequestCheckMate) request);
		else if(request instanceof RequestSurrender)
			requestSurrender((RequestSurrender) request);
	}

	private void requestSurrender(RequestSurrender request) throws IOException {
		for (Game game : gameList) {
			if(game.id == request.id){
				game.surrender(request.user);
			}
		}	
	}

	private void requestCheckMate(RequestCheckMate request) throws IOException {	
		for (Game game : gameList) {
			if(game.id == request.id){
				game.checkmate(request.user);
			}
		}		
	}
	
	private void requestMove(RequestMove request) {	
		for (Game game : gameList) {
			if(game.id == request.id){
				game.move(request.boccupied, request.woccupied, request.user);
			}
		}		
	}

	private void requestChatGame(RequestChatGame request) {
		for (Game game : gameList) {
			if(game.id == request.id){
				System.out.println(game.id);
				game.chat(request.chat, request.username);
			}
		}		
	}

	private void requestColor(RequestColor request) {
		for (Game game : gameList) {
			if(game.id == request.id){
				System.out.println(game.id);
				game.color(request.color, request.username);
			}
		}	
	}

	private void requestRPS(RequestRPS request) {
		System.out.println(request.id);
		for (Game game : gameList) {
			if(game.id == request.id)
				game.rps(request.rps, request.username);
		}
	}

	private void requestInviteAnswer(RequestInviteAnswer request) {
		if(request.answer){
			gameList.add(new Game(request.users, gameid++));
		}
	}

	private void requestChat(RequestChat request) throws IOException {
		for (ClientThread clientThread : Server.clientThreads) {
			for (String user : request.users) {
				if(clientThread.getUser().getUsername().equals(user)){
					clientThread.getOut().writeObject(new ResponseChat(request.getUser().getUsername()+": "+request.message,request.users));
					WindowMain.writeInfo("Sending to "+clientThread.getUser().getUsername());
				}
			}
		}

	}

	private void requestInvite(RequestInvite request) throws IOException {
		for (ClientThread clientThread : Server.clientThreads) {
			if(clientThread.getUser().getUsername().equals(request.user)){
				clientThread.getOut().writeObject(new ResponseInvite(request.getUser().getUsername(), request.user));
				WindowMain.writeInfo("Sending to "+clientThread.getUser().getUsername());
			}
		}

	}

	private void requestFriendAnswer(RequestFriendAnswer request) throws IOException{
		Server.dbConn.friendRequestAnswer(request);

		for (ClientThread ct : Server.clientThreads){
			if(request.getSender().getUsername().equals(ct.getUser().getUsername()))
				ct.out.writeObject(new ResponseFindFriend(Server.dbConn.findFriends(new RequestFindFriend(null),0, ct.getUser().getId()),Server.dbConn.findFriends(new RequestFindFriend(null),1,ct.getUser().getId()),false));
			else if(request.getReceiver().getUsername().equals(ct.getUser().getUsername()))
				ct.out.writeObject(new ResponseFindFriend(Server.dbConn.findFriends(new RequestFindFriend(null),0, ct.getUser().getId()),Server.dbConn.findFriends(new RequestFindFriend(null),1,ct.getUser().getId()),false));
		}
		

	}

	private void requestAddFriend(RequestAddFriend request) {
		Server.dbConn.addFriendRequest(request.getUser(), request.friend);
		Server.dbConn.sendFriendRequests();
	}

	private void requestFindFriend(RequestFindFriend request) throws IOException {
		if(request.getSearch()==null)
			request.getOut().writeObject(new ResponseFindFriend(Server.dbConn.findFriends(request,0, -1),Server.dbConn.findFriends(request,1, -1),false));
		else
			request.getOut().writeObject(new ResponseFindFriend(Server.dbConn.findFriends(request,0, -1),Server.dbConn.findFriends(request,1, -1),true));
	}

	private void requestLogin(RequestLogin request) throws IOException{

		String password = String.valueOf(request.password);
		int id = Server.dbConn.checkUser(request.username);

		if(id != 0 && request.isRegistered){			
			if(Server.dbConn.checkPassword(password, request.username)){
				boolean flag = true;

				for (User user : Server.onlineUsers) {
					if(user.getId() == id)
						flag = false;
				}

				if(flag){
					for (ClientThread clientThread : Server.clientThreads) {
						if(clientThread.getOut() == request.getOut()){
							clientThread.getUser().setUsername(request.username);
							clientThread.getUser().setId(id);
							Server.onlineUsers.add(clientThread.getUser());
						}
					}

					WindowMain.writeInfo("Login : "+request.username);
					request.getOut().writeObject(new ResponseBoolean(true,request.isRegistered));
					Server.dbConn.sendFriendRequests();
					for (User user : Server.onlineUsers)
						for (ClientThread ct : Server.clientThreads)
							if(user.getUsername().equals(ct.getUser().getUsername()))
								ct.out.writeObject(new ResponseFindFriend(Server.dbConn.findFriends(new RequestFindFriend(null),0, user.getId()),Server.dbConn.findFriends(new RequestFindFriend(null),1,user.getId()),false));

				}

				else{
					request.getOut().writeObject(new ResponseBoolean(false,request.isRegistered,false));

				}
			}
			else{
				request.getOut().writeObject(new ResponseBoolean(false,request.isRegistered));
			}
		}

		else if(id != 0 &&  !request.isRegistered){
			request.getOut().writeObject(new ResponseBoolean(false,request.isRegistered));
		}

		else if(request.isRegistered){
			request.getOut().writeObject(new ResponseBoolean(false,request.isRegistered));
		}

		else{
			Server.dbConn.addUser(request.username,password);
			WindowMain.writeInfo("New Registered User : "+request.username);
			request.getOut().writeObject(new ResponseBoolean(true,request.isRegistered));
		}

	}
}